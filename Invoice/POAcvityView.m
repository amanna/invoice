//
//  POAcvityView.m
//  Pop
//
//  Created by Praveen on 07/09/13.
//  Copyright (c) 2013 Praveen@Tulieservices.com. All rights reserved.
//

#import "POAcvityView.h"
#import <QuartzCore/QuartzCore.h>

@interface POAcvityView(PrivateMethods)
- (void)prepareView;
@end

@implementation POAcvityView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
#pragma mark PUBLIC METHODS

//Initiazes with title
- (id)initWithTitle:(NSString *)theTitle message:(NSString *)theMessage
{
	CGRect frame = CGRectMake(0,0,[[UIScreen mainScreen] bounds].size.width,[[UIScreen mainScreen] bounds].size.height);
	
	if (self = [super initWithFrame:frame])
	{
		
				
		[self prepareView];
	}
	
	return self;
	
}
//Shows the view
- (void) showView
{
	[[UIApplication sharedApplication] beginIgnoringInteractionEvents];
	
	parentView			= [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
	
	UIWindow *window	= [[UIApplication sharedApplication]keyWindow];
	parentView.center	= window.center;
	[parentView setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8]];
	
	[parentView addSubview:self];
	[window addSubview:parentView];
}
// Hides the view
- (void) hideView
{
	[parentView removeFromSuperview];
	
	parentView = nil;
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    [self addTransitionAnimationOfType:kCATransitionFade forLayer:[[[UIApplication sharedApplication] keyWindow] layer] forDuration:0.30 onTarget:nil];
    
}
#pragma mark PRIVATE METHODS
- (void)addTransitionAnimationOfType:(NSString*)animationType
                            forLayer:(id)layer
                         forDuration:(CFTimeInterval) duration
                            onTarget:(id)delegate
{
    CATransition *animation = [CATransition animation];
    
    [animation setDelegate:delegate];
    [animation setType:animationType];
    [animation setDuration:duration];
    [animation setSubtype:kCATransitionFromRight];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    
    [layer addAnimation:animation forKey:@"fadeViewAnimation"];
}

// Creates the view
- (void)prepareView
{
	[self setBackgroundColor:[UIColor clearColor]];
	[self setUserInteractionEnabled:YES];
	
	UIActivityIndicatorView *progressIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(self.frame.size.width/2, self.frame.size.height/2, 30, 30)];
	[progressIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
	[progressIndicator startAnimating];
	[self addSubview:progressIndicator];
	
	
	UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 255, 280, 50)];
	[messageLabel setTextColor:[UIColor whiteColor]];
	[messageLabel setBackgroundColor:[UIColor clearColor]];
	messageLabel.text = message;
	messageLabel.textAlignment = NSTextAlignmentCenter;
	[self addSubview:messageLabel];
	
}



@end
