//
//  AgentListVC.m
//  Invoice
//
//  Created by Aditi on 19/03/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "AgentListVC.h"
#import "WebServiceManager.h"
#import "POAcvityView.h"
#import "Agent.h"
#import "InvoiceEdit.h"
#import "InvoiceHeaderCell.h"
#import "InvoiceSpreadCell.h"
#import "AddAgentVC.h"

@interface AgentListVC (){
   POAcvityView *activity;
}
- (IBAction)btnBackAction:(UIButton *)sender;
- (IBAction)btnAddEmp:(UIButton *)sender;
@property (nonatomic)NSMutableArray *arrHeaderList;
@property (weak, nonatomic) IBOutlet UICollectionView *myCollection;
@property(nonatomic)NSMutableArray *arrAgentList;

@property(nonatomic)NSInteger editTag;
@end

@implementation AgentListVC
- (void)viewDidAppear:(BOOL)animated{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    NSString *strUid = [prefs stringForKey:@"uid"];
    [activity showView];
    [WebServiceManager getAgentList:strUid onCompletion:^(id object, NSError *error) {
        [activity hideView];
        if(object){
            self.arrAgentList = object;
            if(self.arrAgentList.count > 0){
                [self.myCollection reloadData];
            }
        }else{
            
        }
    }];
    

}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.arrAgentList = [[NSMutableArray alloc]init];
    [self.myCollection registerClass:[InvoiceHeaderCell class] forCellWithReuseIdentifier:@"CellH"];
    [self.myCollection registerNib:[UINib nibWithNibName:@"InvoiceHeaderCell" bundle:nil] forCellWithReuseIdentifier:@"CellH"];
    [self.myCollection registerClass:[InvoiceSpreadCell class] forCellWithReuseIdentifier:@"Cell"];
    [self.myCollection registerNib:[UINib nibWithNibName:@"InvoiceSpreadCell" bundle:nil] forCellWithReuseIdentifier:@"Cell"];
    
    [self.myCollection registerClass:[InvoiceEdit class] forCellWithReuseIdentifier:@"CellE"];
    [self.myCollection registerNib:[UINib nibWithNibName:@"InvoiceEdit" bundle:nil] forCellWithReuseIdentifier:@"CellE"];
    
    
    self.arrHeaderList = [[NSMutableArray alloc]initWithObjects:@"Agent Name",@"Email",@" ",nil];
    activity = [[POAcvityView alloc]initWithTitle:@"Loading.." message:@"Loading.."];
   
    // Do any additional setup after loading the view.
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.arrHeaderList.count;//no of row
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return self.arrAgentList.count +1;//no of column
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CGFloat x=(self.myCollection.frame.size.width -6)/3 ;
   return CGSizeMake(x , 50);
    
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0,0,3,3);//top - left - bottom- right
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section==0){
        InvoiceHeaderCell *cellH = [collectionView dequeueReusableCellWithReuseIdentifier:@"CellH" forIndexPath:indexPath];
        if(!cellH){
            [collectionView registerNib:[UINib nibWithNibName:@"InvoiceHeaderCell" bundle:nil] forCellWithReuseIdentifier:@"CellH"];
            [collectionView dequeueReusableCellWithReuseIdentifier:@"CellH" forIndexPath:indexPath];
        }
        NSLog(@"section=%ld&row=%ld",(long)indexPath.section,(long)indexPath.row);
        NSString *str= [self.arrHeaderList objectAtIndex:indexPath.row];
        cellH.lblHeader.text = str;
        cellH.backgroundColor=[UIColor whiteColor];
        return cellH;
        
    }else{
        if(indexPath.row==2){
            InvoiceEdit *cellE = [collectionView dequeueReusableCellWithReuseIdentifier:@"CellE" forIndexPath:indexPath];
            if(!cellE){
                [collectionView registerNib:[UINib nibWithNibName:@"InvoiceEdit" bundle:nil] forCellWithReuseIdentifier:@"CellE"];
                [collectionView dequeueReusableCellWithReuseIdentifier:@"CellE" forIndexPath:indexPath];
            }
            
            cellE.btnEdit.tag = indexPath.section -1;
            cellE.btnDelete.tag = indexPath.section -1;
            cellE.backgroundColor=[UIColor whiteColor];
            [cellE.btnEdit addTarget:self action:@selector(btnEditAction:) forControlEvents:UIControlEventTouchUpInside];
            [cellE.btnDelete addTarget:self action:@selector(btnDeleteAction:) forControlEvents:UIControlEventTouchUpInside];
            NSLog(@"section=%ld&row=%ld",(long)indexPath.section,(long)indexPath.row);
            return cellE;
        }else{
            InvoiceSpreadCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
            if(!cell){
                [collectionView registerNib:[UINib nibWithNibName:@"InvoiceSpreadCell" bundle:nil] forCellWithReuseIdentifier:@"Cell"];
                [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
            }
            cell.backgroundColor=[UIColor whiteColor];
            NSString *str;
            Agent *emp = [self.arrAgentList objectAtIndex:indexPath.section -1];
            if(indexPath.row==0){
                str = emp.strName;
            }else if (indexPath.row==1){
                str = emp.strEmail;
            }
            cell.lblHeader.text = str;
            return cell;
        }
    }
    
}

- (void)btnDeleteAction:(UIButton*)sender{
    [activity showView];
    Agent *emp = [self.arrAgentList objectAtIndex:sender.tag];
    [WebServiceManager deleteAgent:emp.strID onCompletion:^(id object, NSError *error) {
        if(object){
            NSString *str = object;
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            // getting an NSString
            NSString *strUid = [prefs stringForKey:@"uid"];
            if([str isEqualToString:@"1"]){
                [WebServiceManager getAgentList:strUid onCompletion:^(id object, NSError *error) {
                    [activity hideView];
                    if(object){
                        
                        self.arrAgentList = object;
                        [self.myCollection reloadData];
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete Agent"
                                                                        message:@"Agent deleted successfully"
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                        [alert show];
                        
                    }else{
                        
                    }
                }];
            }
            
            
            
            
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete Agent"
                                                            message:@"Some Problem Occurs!!Please try again later"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    }];

}
- (IBAction)btnBackAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segueAddAgent"])
    {
       
        AddAgentVC *add =
        [segue destinationViewController];
        add.isEdit = NO;
        

    }else{
        AddAgentVC *add = [segue destinationViewController];
        add.isEdit = YES;
        add.agent = [self.arrAgentList objectAtIndex:self.editTag];
    }
}

- (void)btnEditAction:(UIButton*)sender{
    
    self.editTag = sender.tag;
    
    [self performSegueWithIdentifier:@"segueEditAgent" sender:self];
    
    
}
- (IBAction)btnAddEmp:(UIButton *)sender {
    
    [self performSegueWithIdentifier:@"segueAddAgent" sender:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
