//
//  Job.m
//  Invoice
//
//  Created by Aditi on 07/04/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "Job.h"

@implementation Job
- (id)initWithDictionary:(NSDictionary*)dict{
   // invoice_number
    
    
    self.arrInvoiceDetails = [[NSMutableArray alloc]init];
    self.arrEvidence = [[NSMutableArray alloc]init];
    
    self.strInvoice = [NSString stringWithFormat:@"%ld",[[dict valueForKey:@"id"]integerValue]];
    
    self.strInvoiceNum = [dict valueForKey:@"invoice_number"];
    
    self.strAddr = [dict valueForKey:@"address"];
    self.strCust = [NSString stringWithFormat:@"%@",[dict valueForKey:@"customer_ref"]];
    self.strEmail = [dict valueForKey:@"email"];
    self.strName = [dict valueForKey:@"name"];
    self.strSubTot = [NSString stringWithFormat:@"%ld",[[dict valueForKey:@"subtotal"]integerValue]];
    self.strTot = [NSString stringWithFormat:@"%ld",[[dict valueForKey:@"total"]integerValue]];
    
    self.strVat = [NSString stringWithFormat:@"%ld",[[dict valueForKey:@"vat"]integerValue]];
    
    self.strsignature = [dict valueForKey:@"signature"];
    
    NSArray *arrEv = [dict valueForKey:@"evidence"];
    
    
    [arrEv enumerateObjectsUsingBlock:^(NSDictionary *dictEv, NSUInteger idx, BOOL * _Nonnull stop) {
        [self.arrEvidence addObject:dictEv];
        
    }];
    
     NSArray *arrInvoice = [dict valueForKey:@"invoicedetails"];
    [arrInvoice enumerateObjectsUsingBlock:^(NSDictionary *dictInvoice, NSUInteger idx, BOOL * _Nonnull stop) {
        [self.arrInvoiceDetails addObject:dictInvoice];
        
    }];
    
    NSDictionary *dictAgent = [dict valueForKey:@"agentdetails"];
    NSString *agentNmae = [dictAgent valueForKey:@"fullname"];
    NSString *agentEmail = [dictAgent valueForKey:@"email"];
    if([agentNmae isKindOfClass:[NSNull class]]){
        
    }else{
        self.strAgentName = agentNmae;
    }
    
    
    NSDictionary *dictDt = [dict valueForKey:@"date"];
    NSString *strDt = [dictDt valueForKey:@"date"];
    if([strDt isKindOfClass:[NSNull class]]){
            }else{
                NSArray *array = [strDt componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                self.strDate = [array objectAtIndex:0];
                
                NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                dateFormatter.dateFormat = @"yyyy-MM-dd";
                
                NSDate *dt = [dateFormatter dateFromString:self.strDate];
                dateFormatter.dateFormat = @"dd/MM/yyyy";
                self.strDate = [dateFormatter stringFromDate:dt];
                

        
    }
    
    
    
    NSDictionary *dictJobDt = [dict valueForKey:@"job_date"];
    NSString *strJobDt = [dictJobDt valueForKey:@"date"];
    if([strJobDt isKindOfClass:[NSNull class]]){
    }else{
        NSArray *array1 = [strJobDt componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        self.strJobDt = [array1 objectAtIndex:0];
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"yyyy-MM-dd";
        
        NSDate *dt = [dateFormatter dateFromString:self.strJobDt];
        dateFormatter.dateFormat = @"dd/MM/yyyy";
        self.strJobDt = [dateFormatter stringFromDate:dt];

    }

    
    return self;
}
@end
