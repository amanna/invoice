//
//  Job.h
//  Invoice
//
//  Created by Aditi on 07/04/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Job : NSObject
@property(nonatomic)NSString *strInvoice;
@property(nonatomic)NSString *strInvoiceNum;
@property(nonatomic)NSString *strName;
@property(nonatomic)NSString *strAddr;

@property(nonatomic)NSString *strCust;
@property(nonatomic)NSString *strAgentName;
@property(nonatomic)NSString *strJobDt;

@property(nonatomic)NSString *strSubTot;
@property(nonatomic)NSString *strVat;
@property(nonatomic)NSString *strTot;

@property(nonatomic)NSString *strDate;
@property(nonatomic)NSString *strEmail;
@property(nonatomic)NSString *strsignature;

@property(nonatomic)NSMutableArray *arrEvidence;
@property(nonatomic)NSMutableArray *arrInvoiceDetails;
- (id)initWithDictionary:(NSDictionary*)dict;
@end
