//
//  ExcelSheetCell.h
//  Invoice
//
//  Created by MAPPS MAC on 02/03/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExcelSheetCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblContent;
@property (weak, nonatomic) IBOutlet UILabel *lblTop;

@end
