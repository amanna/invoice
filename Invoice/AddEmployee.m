//
//  AddEmployee.m
//  Invoice
//
//  Created by MAPPS MAC on 14/03/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "AddEmployee.h"
#import "WebServiceManager.h"
#import "POAcvityView.h"

@interface AddEmployee (){
    POAcvityView *activity;
}
@property (weak, nonatomic) IBOutlet UIButton *btnAdmin;
- (IBAction)btnAdminAction:(UIButton *)sender;

@end

@implementation AddEmployee
//The event handling method
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    [self.view endEditing:true];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [singleFingerTap setCancelsTouchesInView:NO];
    [self.view addGestureRecognizer:singleFingerTap];
    activity = [[POAcvityView alloc]initWithTitle:@"Loading.." message:@"Loading.."];
    if(self.isEdit==YES){
        self.txtFname.text = self.emp.strFName;
        self.txtLname.text = self.emp.strLName;
        self.txtEmail.text = self.emp.strEmail;
        self.txtPass.text = self.emp.strPass;
        self.txtUserNmae.text = self.emp.strUserName;
        self.lblTitle.text = @"Edit Employee";
        
        if([self.emp.strAdmin isEqualToString:@"Y"]){
            self.btnAdmin.tag = 100;
           [self.btnAdmin setBackgroundImage:[UIImage imageNamed:@"checked_checkbox.png"] forState:UIControlStateNormal];
            
        }else{
            self.btnAdmin.tag = 200;
            [self.btnAdmin setBackgroundImage:[UIImage imageNamed:@"unchecked_checkbox.png"] forState:UIControlStateNormal];
            
        }
    }else{
         self.lblTitle.text = @"Add Employee";
    }
       // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnSubmitAction:(UIButton *)sender {
    if([self.txtFname.text isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Entry"
                                                        message:@"Please enter first name"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }else if([self.txtLname.text isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Entry"
                                                        message:@"Please enter last name"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];

    }else if([self.txtEmail.text isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Entry"
                                                        message:@"Please enter email"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];

    }else if([self.txtUserNmae.text isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Entry"
                                                        message:@"Please enter user name"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];

    }else if([self.txtPass.text isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Entry"
                                                        message:@"Please enter password"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];

    }else{
        if(self.isEdit == YES){
            [activity showView];
            
            NSString *strEditEmp = [NSString stringWithFormat:@"userid=%@&firstname=%@&lastname=%@&username=%@&email=%@&password=%@&is_admin=%@​",self.emp.strID,self.txtFname.text,self.txtLname.text,self.txtUserNmae.text,self.txtEmail.text,self.txtPass.text,self.strAdmin];
            
            [WebServiceManager EditEmployee:strEditEmp onCompletion:^(id object, NSError *error) {
                if(object){
                    [activity hideView];
                    NSString *str = object;
                     if ([str isEqualToString:@"1"]){
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Entry"
                                                                        message:@"Employee Updation successfully"
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                         alert.tag =100;
                        [alert show];
                    }
                }else{
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Entry"
                                                                    message:@"Some Problem Occurs!!Please try again later"
                                                                   delegate:self
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    [alert show];
                }
            }];
        }else{
//            NSString *strAddEmp = [NSString stringWithFormat:@"firstname=%@&lastname=%@&username=%@&email=%@&password=%@",@"Aditi",@"Manna",@"amanna",@"job2012.aditi@gmail.com",@"123456@"];
            
            [activity showView];
             NSString *strAddEmp = [NSString stringWithFormat:@"firstname=%@&lastname=%@&username=%@&email=%@&password=%@",self.txtFname.text,self.txtLname.text,self.txtUserNmae.text,self.txtEmail.text,self.txtPass.text];
            
            [WebServiceManager addEmployee:strAddEmp onCompletion:^(id object, NSError *error) {
                if(object){
                    [activity hideView];
                    NSString *str = object;
                    if([str isEqualToString:@"2"]){
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Entry"
                                                                        message:@"This Employee already exists"
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                        [alert show];
                    }else if ([str isEqualToString:@"1"]){
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Entry"
                                                                        message:@"Employee entry successful"
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                        alert.tag = 99;
                        [alert show];
                        
                    }else if ([str isEqualToString:@"0"]){
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Entry"
                                                                        message:@"Some Problem Occurs!!Please try again later"
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                        [alert show];
                    }

                }else{
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Entry"
                                                                    message:@"Some Problem Occurs!!Please try again later"
                                                                   delegate:self
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    [alert show];

                }
            }];
        }

    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 99 || alertView.tag == 100 )
    {
        if (buttonIndex == 0)
        {
           // [self performSegueWithIdentifier:@"segueEmpList" sender:self];
            [self.navigationController popViewControllerAnimated:YES];
            
        }
    }
}
- (IBAction)btnCancelAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    if(textField==self.txtEmail){
        NSString *emailReg = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailReg];
        
        if ([emailTest evaluateWithObject:textField.text] == NO)
        {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Enter valid Email" message:@"Please Enter Valid Email Address." delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
            [alert show];
            
        }
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField==self.txtFname){
        [self.txtFname resignFirstResponder];
         [self.txtLname becomeFirstResponder];
    }else if (textField==self.txtLname){
        [self.txtLname resignFirstResponder];
        [self.txtEmail becomeFirstResponder];
    }else if (textField==self.txtEmail){
        [self.txtEmail resignFirstResponder];
        [self.txtUserNmae becomeFirstResponder];
    }else if (textField==self.txtUserNmae){
        [self.txtUserNmae resignFirstResponder];
        [self.txtPass becomeFirstResponder];
    }else if (textField==self.txtPass){
        [self.txtPass resignFirstResponder];
       
    }
    return YES;
}
- (IBAction)btnAdminAction:(UIButton *)sender {
    if(sender.tag ==200){
        self.btnAdmin.tag = 100;
        [self.btnAdmin setBackgroundImage:[UIImage imageNamed:@"checked_checkbox.png"] forState:UIControlStateNormal];
        self.strAdmin = @"Y";
        
    }else{
        self.btnAdmin.tag = 200;
        [self.btnAdmin setBackgroundImage:[UIImage imageNamed:@"unchecked_checkbox.png"] forState:UIControlStateNormal];
        self.strAdmin = @"N";
        
    }

}
@end
