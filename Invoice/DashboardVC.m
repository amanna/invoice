//
//  DashboardVC.m
//  Invoice
//
//  Created by MAPPS MAC on 24/02/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "DashboardVC.h"
#import "AppConstants.h"
@interface DashboardVC ()
- (IBAction)btnInvoiceAction:(UIButton *)sender;
- (IBAction)btnAdminAction:(UIButton *)sender;
- (IBAction)btnJobListAction:(UIButton *)sender;
- (IBAction)btnLogoutAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnEmp;
- (IBAction)btnEmpAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnAgent;
- (IBAction)btnAgentAction:(UIButton *)sender;
- (IBAction)btnSettingsAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnSettings;
@property (weak, nonatomic) IBOutlet UILabel *lblName;

@end

@implementation DashboardVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // getting an NSString
    NSString *isAdmin = [prefs stringForKey:@"isAdmin"];
    if([isAdmin isEqualToString:@"1"]){
        
        self.btnEmp.hidden = NO;
        self.btnSettings.hidden = NO;
    }else{
       
        self.btnEmp.hidden = YES;
         self.btnSettings.hidden = YES;
    }

    self.lblName.text = [prefs stringForKey:@"fname"];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnInvoiceAction:(UIButton *)sender {
    [self performSegueWithIdentifier:@"segueInvoice" sender:self];
}



- (IBAction)btnJobListAction:(UIButton *)sender {
    [self performSegueWithIdentifier:@"segueJobsList" sender:self];
}

- (IBAction)btnLogoutAction:(UIButton *)sender {
       [self.navigationController popToRootViewControllerAnimated:YES];
}
- (IBAction)btnEmpAction:(UIButton *)sender {
     [self performSegueWithIdentifier:@"segueEmpList" sender:self];
}
- (IBAction)btnAgentAction:(UIButton *)sender {
    [self performSegueWithIdentifier:@"segueAgentList" sender:self];
}

- (IBAction)btnSettingsAction:(UIButton *)sender {
    [self performSegueWithIdentifier:@"segueSettings" sender:self];
    
}
@end
