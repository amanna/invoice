//
//  Employee.m
//  Invoice
//
//  Created by Aditi on 15/03/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "Employee.h"

@implementation Employee
- (id)initWithDictionary:(NSDictionary*)dict{
    self.strEmail = [dict valueForKey:@"email"];
    self.strUserName = [dict valueForKey:@"username"];
    self.strPass = [dict valueForKey:@"password"];
    self.strID = [dict valueForKey:@"id"];
    self.strAdmin = [dict valueForKey:@"is_admin"];
    NSString *strFullName = [dict valueForKey:@"fullname"];
    NSArray *array = [strFullName componentsSeparatedByString:@" "];
    self.strFName = [array objectAtIndex:0];
    self.strLName = [array objectAtIndex:1];
    return self;
}
//email = "dibya@gmail.com";
//fullname = "Dibya Mitra";
//id = 6;
//image = "";
//username = dibya;
@end
