//
//  LoginVC.m
//  Invoice
//
//  Created by MAPPS MAC on 24/02/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "LoginVC.h"
#import "WebServiceManager.h"
#import "POAcvityView.h"
@interface LoginVC ()<UITextFieldDelegate>{
     POAcvityView *activity;
}
@property(nonatomic)NSString *strEmail;
@property(nonatomic)NSString *strPass;
@property (weak, nonatomic) IBOutlet UITextField *txtUserNmae;

@property (weak, nonatomic) IBOutlet UITextField *txtPass;
- (IBAction)btnLoginAction:(UIButton *)sender;

@end

@implementation LoginVC
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    [self.view endEditing:true];
}
- (void)viewDidAppear:(BOOL)animated{
    self.txtUserNmae.text = @"";
    self.txtPass.text = @"";
}
- (void)viewDidLoad {
    [super viewDidLoad];
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [singleFingerTap setCancelsTouchesInView:NO];
    [self.view addGestureRecognizer:singleFingerTap];
    activity = [[POAcvityView alloc]initWithTitle:@"Loading.." message:@"Loading.."];
//    [WebServiceManager loginOnCompletion:<#(NSString *)#> andPass:<#(NSString *)#> onCompletion:^(id object, NSError *error) {
//        
//    }];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
  
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField==self.txtUserNmae){
        [self.txtPass becomeFirstResponder];
         [self.txtUserNmae resignFirstResponder];
    }else{
        [self.txtPass resignFirstResponder];
    }
    return YES;
}
- (IBAction)btnLoginAction:(UIButton *)sender {
    
//  [self performSegueWithIdentifier:@"segueDashboard" sender:self];
   if ([self.txtUserNmae.text isEqualToString:@""]){
       UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login"
                                                       message:@"Please enter User Name"
                                                      delegate:self
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil];
       [alert show];
       
   }else if ([self.txtPass.text isEqualToString:@""]){
       UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login"
                                                       message:@"Please enter Password"
                                                      delegate:self
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil];
       [alert show];
       
   }else{
     [activity showView];
       self.strEmail = self.txtUserNmae.text;
       self.strPass = self.txtPass.text;
       [WebServiceManager loginOnCompletion:self.strEmail andPass:self.strPass onCompletion:^(id object, NSError *error) {
           [activity hideView];
           if(object){
               NSString *str = object;
               if([str isEqualToString:@"1"]){
                   [self performSegueWithIdentifier:@"segueCreateAgent" sender:self];
               }else{
                   UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login"
                                                                   message:@"Invalid Username or Password"
                                                                  delegate:self
                                                         cancelButtonTitle:@"OK"
                                                         otherButtonTitles:nil];
                   [alert show];
               }
               
           }else{
               UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login"
                                                               message:@"Some Problem occurs!!Please try again later!!"
                                                              delegate:self
                                                     cancelButtonTitle:@"OK"
                                                     otherButtonTitles:nil];
               [alert show];
               
           }
       }];
       

       
   }

    
}
@end
