//
//  InvoiceEditVC.h
//  Invoice
//
//  Created by Aditi on 10/04/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Job.h"
@interface InvoiceEditVC : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UITextView *txtAddr;
@property (weak, nonatomic) IBOutlet UITextField *txtCustomRef;

@property (weak, nonatomic) IBOutlet UIButton *btnJobDt;
@property (weak, nonatomic) IBOutlet UIButton *btnAgent;

@property(nonatomic)NSString *strAgent;
@property(nonatomic)NSString *strName;
@property(nonatomic)NSString *strEmpID;
@property(nonatomic)NSString *strCustomRef;
@property(nonatomic)NSString *strAddress;
@property(nonatomic)NSString *strJobDate;
@property(nonatomic)NSString *strDate;
@property(nonatomic)NSString *strEmail;


@property(nonatomic)NSMutableArray *arrInvoiceDetails;
@property(nonatomic)NSMutableArray *arrEvidence;

@property(nonatomic)Job *job;
@end
