//
//  SettingsVC.m
//  Invoice
//
//  Created by Aditi on 21/04/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "SettingsVC.h"
#import "POAcvityView.h"
#import "AppConstants.h"
#import "WebServiceManager.h"
@interface SettingsVC (){
    POAcvityView *activity;
}
@property (weak, nonatomic) IBOutlet UITextField *txtComName;
@property (weak, nonatomic) IBOutlet UITextField *txtAccount;
@property (weak, nonatomic) IBOutlet UITextField *txtSort;
@property (weak, nonatomic) IBOutlet UITextView *txtMsg;
@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
- (IBAction)btnChangeAction:(UIButton *)sender;
- (IBAction)btnSubmitAction:(UIButton *)sender;
- (IBAction)btnCancelAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@end

@implementation SettingsVC
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    [self.view endEditing:true];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleFingerTap];
     activity = [[POAcvityView alloc]initWithTitle:@"Loading.." message:@"Loading.."];
    [activity showView];
    NSString *strLoginUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,kSettingsDetails];
    // the server url to which the image (or the media) is uploaded. Use your server url here
    NSURL* requestURL = [NSURL URLWithString:strLoginUrl];
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:30];
    [request setHTTPMethod:@"GET"];
    [request setURL:requestURL];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data1, NSError *connectionError) {
        if(data1)
        {
            
            NSError *jsonParserError;
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data1 options:kNilOptions error:&jsonParserError];
            // NSLog(@"res ::%@", returnString);
           
            NSInteger status = [[[json valueForKey:@"result"]valueForKey:@"status"]integerValue];
            if(status==1){
                
                
                NSDictionary *dict = [[json valueForKey:@"result"] valueForKey:@"settingsdtl"];
                
                self.txtComName.text = [dict valueForKey:@"company_name"];
                self.txtAccount.text = [dict valueForKey:@"account_no"];
                self.txtSort.text = [dict valueForKey:@"sort_code"];
                self.txtMsg.text = [dict valueForKey:@"message"];
                
                dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
                dispatch_async(queue, ^{
                    NSURL *imageURL= [NSURL URLWithString:[dict valueForKey:@"company_logo"]];
                    NSData *data = [NSData dataWithContentsOfURL:imageURL];
                    UIImage *image = [UIImage imageWithData:data];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [activity hideView];
                        self.imgLogo.image = image;
                    });
                });
                
                //company_logo
                
            }else{
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Settings"
                                                                message:@"Some Problem Occur!!!Please try again later!!"
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
            
            
            
            
        }
        else{
            
        }
        // block(NO);
    }];
    


    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField==self.txtComName){
        [self.txtAccount becomeFirstResponder];
        [self.txtComName resignFirstResponder];
    }else if (textField==self.txtAccount){
        [self.txtSort becomeFirstResponder];
        [self.txtAccount resignFirstResponder];
    }
    else{
        [self.txtSort resignFirstResponder];
        [self.txtMsg becomeFirstResponder];
    }
    return YES;
}
- (void) textViewDidBeginEditing:(UITextView *) textView {
    [self.txtMsg becomeFirstResponder];
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    [self.txtMsg resignFirstResponder];
}
- (void)addSettings{
    [activity showView];
    
    
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
    
    [ _params setObject:self.txtComName.text forKey:@"company_name"];
    [ _params setObject:self.txtAccount.text forKey:@"account_no"];
    [ _params setObject:self.txtSort.text forKey:@"sort_code"];
    [ _params setObject:self.txtMsg.text forKey:@"message"];
    
    // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
    NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
    
    // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
    NSString* FileParamConstant = @"file";
    
    NSString *strLoginUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,kSettings];
    // the server url to which the image (or the media) is uploaded. Use your server url here
    NSURL* requestURL = [NSURL URLWithString:strLoginUrl];
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:30];
    [request setHTTPMethod:@"POST"];
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    
    //Save Signature
    
    UIImage *vwImage = self.imgLogo.image;
    NSData *imageData = UIImageJPEGRepresentation(vwImage, 0.2f);
    
    if (imageData) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", @"company_logo"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imageData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    NSString *postLength = [NSString stringWithFormat:@"%d", [body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setURL:requestURL];
    
    
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data1, NSError *connectionError) {
        if(data1)
        {
            //  NSString *returnString = [[NSString alloc] initWithData:data1 encoding:NSUTF8StringEncoding];
            //NSDictionary *res = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            NSError *jsonParserError;
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data1 options:kNilOptions error:&jsonParserError];
            // NSLog(@"res ::%@", returnString);
            
            NSInteger status = [[[json valueForKey:@"result"]valueForKey:@"status"]integerValue];
            if(status==1){
                
                NSDictionary *dict = [[json valueForKey:@"result"] valueForKey:@"settingsdtl"];
                
                self.txtComName.text = [dict valueForKey:@"company_name"];
                self.txtAccount.text = [dict valueForKey:@"account_no"];
                self.txtSort.text = [dict valueForKey:@"sort_code"];
                self.txtMsg.text = [dict valueForKey:@"message"];
                
                dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
                dispatch_async(queue, ^{
                    NSURL *imageURL= [NSURL URLWithString:[dict valueForKey:@"company_logo"]];
                    NSData *data = [NSData dataWithContentsOfURL:imageURL];
                    UIImage *image = [UIImage imageWithData:data];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [activity hideView];
                        self.imgLogo.image = image;
                    });
                });

                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Settings"
                                                                message:@"Settings updated successfully"
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                
                [alert show];
                
            }else{
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Settings"
                                                                message:@"Some Problem Occur!!!Please try again later!!"
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
            
            
            
            
        }
        else{
            
        }
        // block(NO);
    }];
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnChangeAction:(UIButton *)sender {
    [self.view endEditing:YES];
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickerController.delegate = self;
    [self presentViewController:imagePickerController animated:YES completion:nil];

}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    //You can retrieve the actual UIImage
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        self.imgLogo.image = image;
           }];
}

- (IBAction)btnSubmitAction:(UIButton *)sender {
    [self.view endEditing:true];
    [self addSettings];
    
}

- (IBAction)btnCancelAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
