//
//  JobsListVC.m
//  Invoice
//
//  Created by Aditi on 27/02/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "JobsListVC.h"
#import "CustomCell.h"
#import "ExcelHeader.h"
#import "ExcelSheetCell.h"
#import "WebServiceManager.h"
#import "Job.h"
#import "POAcvityView.h"
#import "InvoiceEvidence.h"
#import "ExcelEvidence.h"
#import "ExcelSpreadDetails.h"
#import "InvoiceEdit.h"
#import "InvoiceEditVC.h"
@interface JobsListVC ()<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>{
    POAcvityView *activity;
}

@property(nonatomic)NSMutableArray *arrHeaderList;
@property(nonatomic)NSMutableArray *arrHeaderSheetList;
@property (weak, nonatomic) IBOutlet UICollectionView *myCollection;
@property(nonatomic)NSString *strEmpID;
@property(nonatomic)NSMutableArray *arrJobList;
- (IBAction)btnDownloadAction:(UIButton *)sender;

@end

@implementation JobsListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    activity = [[POAcvityView alloc]initWithTitle:@"Loading.." message:@"Loading.."];
    self.arrJobList = [[NSMutableArray alloc]init];
    [self.myCollection registerClass:[ExcelSpreadDetails class] forCellWithReuseIdentifier:@"CellD"];
    [self.myCollection registerClass:[ExcelSheetCell class] forCellWithReuseIdentifier:@"Cell"];
    [self.myCollection registerClass:[ExcelHeader class] forCellWithReuseIdentifier:@"CellH"];
    [self.myCollection registerClass:[ExcelEvidence class] forCellWithReuseIdentifier:@"CellE"];
    [self.myCollection registerClass:[InvoiceEvidence class] forCellWithReuseIdentifier:@"CellE"];
    
    [self.myCollection registerNib:[UINib nibWithNibName:@"ExcelHeader" bundle:nil] forCellWithReuseIdentifier:@"CellH"];
    [self.myCollection registerNib:[UINib nibWithNibName:@"ExcelSheet" bundle:nil] forCellWithReuseIdentifier:@"Cell"];
    [self.myCollection registerNib:[UINib nibWithNibName:@"ExcelEvidence" bundle:nil] forCellWithReuseIdentifier:@"CellE"];
    [self.myCollection registerNib:[UINib nibWithNibName:@"ExcelEvidence" bundle:nil] forCellWithReuseIdentifier:@"CellE"];
     [self.myCollection registerNib:[UINib nibWithNibName:@"ExcelSpreadDetails" bundle:nil] forCellWithReuseIdentifier:@"CellD"];
    
    self.arrHeaderList = [[NSMutableArray alloc]initWithObjects:@"Invoice No",@"Date",@"Job date",@"Agent Name", @"Name", @"Address",@"Customer Ref",@"Subtotal",@"Vat",@"Total",@"Email Delivery Address",@"Customer Signature",@" ",nil];
    

    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *strUid = [prefs stringForKey:@"uid"];
    NSString *strAdmin = [prefs stringForKey:@"isAdmin"];
    
//    if([strAdmin isEqualToString:@"1"]){
//         self.strEmpID = strUid;
//    }else{
//         self.strEmpID = @"";
//    }
    
   self.strEmpID = strUid;
    [activity showView];
    [WebServiceManager getInvoiceList:self.strEmpID onCompletion:^(id object, NSError *error) {
        [activity hideView];
        if(object){
            self.arrJobList = object;
            [self.myCollection reloadData];
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Invoice List"
                                                            message:@"Some Problem Occurs!!!Please try again later"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];

        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.arrJobList.count +1;//no of row
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return self.arrHeaderList.count;//no of column
}
- (void)btnViewDetailsAction:(UIButton*)sender{
    Job *job = [self.arrJobList objectAtIndex:sender.tag];
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    InvoiceEditVC *add = [storyboard instantiateViewControllerWithIdentifier:@"InvoiceEditVC"];
    add.job = job;
    [self presentViewController:add
                       animated:YES
                     completion:nil];

}

- (void)btnDeleteAction:(UIButton*)sender{
    Job *job = [self.arrJobList objectAtIndex:sender.tag];
    [activity showView];
   
    [WebServiceManager deleteInvoice:job.strInvoice onCompletion:^(id object, NSError *error) {
        
        if(object){
            NSString *str = object;
            if([str isEqualToString:@"1"]){
                [WebServiceManager getInvoiceList:self.strEmpID onCompletion:^(id object, NSError *error) {
                    [activity hideView];
                    if(object){
                        
                        self.arrJobList = object;
                        [self.myCollection reloadData];
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete Invoice"
                                                                        message:@"Invoice deleted successfully"
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                        [alert show];
                        
                    }else{
                        
                    }
                }];
            }
            
            
            
            
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete Invoice"
                                                            message:@"Some Problem Occurs!!Please try again later"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    }];

    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row==0){
        ExcelHeader *cellH = [collectionView dequeueReusableCellWithReuseIdentifier:@"CellH" forIndexPath:indexPath];
        if(!cellH){
            [collectionView registerNib:[UINib nibWithNibName:@"ExcelHeader" bundle:nil] forCellWithReuseIdentifier:@"CellH"];
            [collectionView dequeueReusableCellWithReuseIdentifier:@"CellH" forIndexPath:indexPath];
        }
        NSString *str= [self.arrHeaderList objectAtIndex:indexPath.section];
        cellH.lblHeader.text = str;
        cellH.backgroundColor=[UIColor whiteColor];
        return cellH;
        
    }else{
         Job *job = [self.arrJobList objectAtIndex:indexPath.row -1];
        NSLog(@"Print Of section=%ld and row=%ld",(long)indexPath.section,(long)indexPath.row);
        if(indexPath.section==11){
           ExcelEvidence *cellE = [collectionView dequeueReusableCellWithReuseIdentifier:@"CellE" forIndexPath:indexPath];
            if(!cellE){
                [collectionView registerNib:[UINib nibWithNibName:@"InvoiceEvidence" bundle:nil] forCellWithReuseIdentifier:@"CellE"];
                [collectionView dequeueReusableCellWithReuseIdentifier:@"CellE" forIndexPath:indexPath];
            }
           
            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
            dispatch_async(queue, ^{
                NSURL *imageURL= [NSURL URLWithString:job.strsignature];
                NSData *data = [NSData dataWithContentsOfURL:imageURL];
                UIImage *image = [UIImage imageWithData:data];
                dispatch_async(dispatch_get_main_queue(), ^{
                    cellE.imgEvidence.image= image;
                });  
            });
            cellE.backgroundColor=[UIColor whiteColor];
            return cellE;

        }else if (indexPath.section==12){
            
            ExcelSpreadDetails *cellD = [collectionView dequeueReusableCellWithReuseIdentifier:@"CellD" forIndexPath:indexPath];
            if(!cellD){
                [collectionView registerNib:[UINib nibWithNibName:@"ExcelSpreadDetails" bundle:nil] forCellWithReuseIdentifier:@"CellD"];
                [collectionView dequeueReusableCellWithReuseIdentifier:@"CellD" forIndexPath:indexPath];
            }
            cellD.btnViewDetails.tag = indexPath.row - 1;
            [cellD.btnViewDetails addTarget:self action:@selector(btnViewDetailsAction:) forControlEvents:UIControlEventTouchUpInside];
            
            
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            // getting an NSString
            NSString *isAdmin = [prefs stringForKey:@"isAdmin"];
            if([isAdmin isEqualToString:@"1"]){
                cellD.btnDelete.hidden = NO;
                cellD.btnDelete.tag = indexPath.row - 1;
                [cellD.btnDelete addTarget:self action:@selector(btnDeleteAction:) forControlEvents:UIControlEventTouchUpInside];
            }else{
               cellD.btnDelete.hidden = YES;
            }
            cellD.backgroundColor=[UIColor whiteColor];
            return cellD;

        }
        else
        {
        ExcelSheetCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        if(!cell){
             [collectionView registerNib:[UINib nibWithNibName:@"ExcelSheet" bundle:nil] forCellWithReuseIdentifier:@"Cell"];
            [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        }
       cell.backgroundColor=[UIColor whiteColor];
        if(indexPath.section==0){
            cell.lblContent.text = job.strInvoiceNum;
        }else if (indexPath.section==1){
             cell.lblContent.text = job.strDate;
        }else if (indexPath.section==2){
             cell.lblContent.text = job.strJobDt;
        }else if (indexPath.section==3){
             cell.lblContent.text = job.strAgentName;
        }else if (indexPath.section==4){
            cell.lblContent.text = job.strName;
        }else if (indexPath.section==5){
            cell.lblContent.text = job.strAddr;
        }else if (indexPath.section==6){
            cell.lblContent.text = job.strCust;
        }else if (indexPath.section==7){
            cell.lblContent.text = job.strSubTot;
        }else if (indexPath.section==8){
            cell.lblContent.text = job.strVat;
        }else if (indexPath.section==9){
            cell.lblContent.text = job.strTot;
        }else if (indexPath.section==10){
            cell.lblContent.text = job.strEmail;
        }
        //cell.lblContent.text = str;
        return cell;
        }
    }
    
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section==0){
        return CGSizeMake(200, 30);
    }else if (indexPath.section==1){
        return CGSizeMake(150, 30);
    }else if (indexPath.section==2){
        return CGSizeMake(160, 30);
    }else if (indexPath.section==3){
        return CGSizeMake(160, 30);
    }else if (indexPath.section==4){
        return CGSizeMake(260, 30);
    }else if (indexPath.section==5){
        return CGSizeMake(300, 30);
    }else if (indexPath.section==6){
        return CGSizeMake(160, 30);
    }else if (indexPath.section==12){
        return CGSizeMake(160, 30);
    }else{
         return CGSizeMake(220, 30);
    }
    
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0,0,0,0);//top - left - bottom- right
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnBackAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btnDownloadAction:(UIButton *)sender {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *strUid = [prefs stringForKey:@"uid"];
    [activity showView];
    self.strEmpID = strUid;
    [WebServiceManager sendExcel:self.strEmpID onCompletion:^(id object, NSError *error) {
        [activity hideView];
        if(object){
            NSString *str = object;
            if([str isEqualToString:@"1"]){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Invoice List"
                                                                message:@"Excel is sent to your mail"
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }else{
                
            }
        }else{
            
        }
    }];
}
@end
