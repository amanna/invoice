//
//  AddEmployee.h
//  Invoice
//
//  Created by MAPPS MAC on 14/03/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Employee.h"
@interface AddEmployee : UIViewController
@property(nonatomic)BOOL isEdit;
@property(nonatomic)NSString *strUid;
@property(nonatomic)NSString *strAdmin;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITextField *txtFname;
@property (weak, nonatomic) IBOutlet UITextField *txtLname;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtUserNmae;
@property (weak, nonatomic) IBOutlet UITextField *txtPass;
- (IBAction)btnSubmitAction:(UIButton *)sender;
- (IBAction)btnCancelAction:(UIButton *)sender;
@property(nonatomic)Employee *emp;
@end
