//
//  CreateInvoiceVC.h
//  Invoice
//
//  Created by MAPPS MAC on 24/02/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ColorPickerViewController.h"
@interface CreateInvoiceVC : UIViewController<ColorPickerDelegate>{
    
}
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UITextView *txtAddr;
@property (weak, nonatomic) IBOutlet UITextField *txtCustomRef;

@property (weak, nonatomic) IBOutlet UIButton *btnJobDt;
@property (weak, nonatomic) IBOutlet UIButton *btnAgent;
@property (nonatomic, strong) UIPopoverController *popover;
@property (nonatomic, strong) ColorPickerViewController *colorPicker;
@property (nonatomic, strong) UIPopoverController *colorPickerPopover;
@property(nonatomic)NSString *strAgent;
@property(nonatomic)NSString *strName;
@property(nonatomic)NSString *strEmpID;
@property(nonatomic)NSString *strCustomRef;
@property(nonatomic)NSString *strAddress;
@property(nonatomic)NSString *strJobDate;
@property(nonatomic)NSString *strDate;
@property(nonatomic)NSString *strEmail;
@property(nonatomic)NSString *strInvoiceNo;



//agentid,empid,customer_ref,name,address,job_date,date,email,
@end
