//
//  EmpListVC.m
//  Invoice
//
//  Created by Aditi on 15/03/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "EmpListVC.h"
#import "WebServiceManager.h"
#import "POAcvityView.h"
#import "Employee.h"
#import "InvoiceEdit.h"
#import "InvoiceHeaderCell.h"
#import "InvoiceSpreadCell.h"
#import "AddEmployee.h"

@interface EmpListVC (){
    POAcvityView *activity;
}
- (IBAction)btnBackAction:(UIButton *)sender;
- (IBAction)btnAddEmp:(UIButton *)sender;
@property (nonatomic)NSMutableArray *arrHeaderList;
@property (weak, nonatomic) IBOutlet UICollectionView *myCollection;
@property(nonatomic)NSMutableArray *arrEmpList;
@property(nonatomic)NSInteger tagEdit;
@end

@implementation EmpListVC
- (void)viewDidAppear:(BOOL)animated{
    [activity showView];
    [WebServiceManager getEmpListonCompletion:^(id object, NSError *error) {
        [activity hideView];
        if(object){
            self.arrEmpList = object;
            [self.myCollection reloadData];
        }else{
            
        }
    }];

}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.arrEmpList = [[NSMutableArray alloc]init];
    [self.myCollection registerClass:[InvoiceHeaderCell class] forCellWithReuseIdentifier:@"CellH"];
    [self.myCollection registerNib:[UINib nibWithNibName:@"InvoiceHeaderCell" bundle:nil] forCellWithReuseIdentifier:@"CellH"];
    [self.myCollection registerClass:[InvoiceSpreadCell class] forCellWithReuseIdentifier:@"Cell"];
    [self.myCollection registerNib:[UINib nibWithNibName:@"InvoiceSpreadCell" bundle:nil] forCellWithReuseIdentifier:@"Cell"];
    
      [self.myCollection registerClass:[InvoiceEdit class] forCellWithReuseIdentifier:@"CellE"];
    [self.myCollection registerNib:[UINib nibWithNibName:@"InvoiceEdit" bundle:nil] forCellWithReuseIdentifier:@"CellE"];
    

    self.arrHeaderList = [[NSMutableArray alloc]initWithObjects:@"First Name",@"Last Name",@"Email",@"UserName",@"Password",@"Admin",@"", nil];
    activity = [[POAcvityView alloc]initWithTitle:@"Loading.." message:@"Loading.."];
        // Do any additional setup after loading the view.
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.arrHeaderList.count;//no of row
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return self.arrEmpList.count +1;//no of column
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CGFloat x=(self.myCollection.frame.size.width -14)/7 ;
    if(indexPath.row==5){
        return CGSizeMake(x , 50);
    }else{
        return CGSizeMake(x , 50);
    }
  
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0,0,3,3);//top - left - bottom- right
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section==0){
             InvoiceHeaderCell *cellH = [collectionView dequeueReusableCellWithReuseIdentifier:@"CellH" forIndexPath:indexPath];
              if(!cellH){
                  [collectionView registerNib:[UINib nibWithNibName:@"InvoiceHeaderCell" bundle:nil] forCellWithReuseIdentifier:@"CellH"];
                  [collectionView dequeueReusableCellWithReuseIdentifier:@"CellH" forIndexPath:indexPath];
              }
              NSLog(@"section=%ld&row=%ld",(long)indexPath.section,(long)indexPath.row);
              NSString *str= [self.arrHeaderList objectAtIndex:indexPath.row];
              cellH.lblHeader.text = str;
              cellH.backgroundColor=[UIColor whiteColor];
              return cellH;

    }else{
        if(indexPath.row==6){
            InvoiceEdit *cellE = [collectionView dequeueReusableCellWithReuseIdentifier:@"CellE" forIndexPath:indexPath];
            if(!cellE){
                [collectionView registerNib:[UINib nibWithNibName:@"InvoiceEdit" bundle:nil] forCellWithReuseIdentifier:@"CellE"];
                [collectionView dequeueReusableCellWithReuseIdentifier:@"CellE" forIndexPath:indexPath];
            }
            
            cellE.btnEdit.tag = indexPath.section -1;
             cellE.btnDelete.tag = indexPath.section -1;
            cellE.backgroundColor=[UIColor whiteColor];
            [cellE.btnEdit addTarget:self action:@selector(btnEditAction:) forControlEvents:UIControlEventTouchUpInside];
            [cellE.btnDelete addTarget:self action:@selector(btnDeleteAction:) forControlEvents:UIControlEventTouchUpInside];
            NSLog(@"section=%ld&row=%ld",(long)indexPath.section,(long)indexPath.row);
            return cellE;
        }else{
            InvoiceSpreadCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
            if(!cell){
                [collectionView registerNib:[UINib nibWithNibName:@"InvoiceSpreadCell" bundle:nil] forCellWithReuseIdentifier:@"Cell"];
                [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
            }
            cell.backgroundColor=[UIColor whiteColor];
            NSString *str;
            Employee *emp = [self.arrEmpList objectAtIndex:indexPath.section -1];
            if(indexPath.row==0){
                str = emp.strFName;
            }else if (indexPath.row==1){
                str = emp.strLName;
            }else if (indexPath.row==2){
                str = emp.strEmail;
            }else if (indexPath.row==3){
                str = emp.strUserName;
            }else if (indexPath.row==4){
                str = emp.strPass;
                
            }else if (indexPath.row==5){
                str = emp.strAdmin;
                
            }
            cell.lblHeader.text = str;
            return cell;
        }
    }

}

- (void)btnDeleteAction:(UIButton*)sender{
    [activity showView];
    Employee *emp = [self.arrEmpList objectAtIndex:sender.tag];
    [WebServiceManager deleteEmployee:emp.strID onCompletion:^(id object, id object1,NSError *error) {
         [activity hideView];
        if(object){
            NSString *str = object;
            if([str isEqualToString:@"1"]){
                self.arrEmpList = object1;
                [self.myCollection reloadData];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete Employee"
                                                                message:@"Employee deleted successfully"
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];

            }else{
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete Employee"
                                                                message:object1
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
           
                        
            
            
            
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete Employee"
                                                            message:@"Some Problem Occurs!!Please try again later"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    }];
    
    
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnBackAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segueAddEmp"])
    {
       
        AddEmployee *add =
        [segue destinationViewController];
        add.isEdit = NO;
        
        
    }else{
        
        AddEmployee *add =
        [segue destinationViewController];
        add.isEdit = YES;
        add.emp = [self.arrEmpList objectAtIndex:self.tagEdit];

    }
}
- (void)btnEditAction:(UIButton*)sender{
    self.tagEdit = sender.tag;
   [self performSegueWithIdentifier:@"segueEditEmp" sender:self];
    
    
}
- (IBAction)btnAddEmp:(UIButton *)sender {
    
    [self performSegueWithIdentifier:@"segueAddEmp" sender:self];
}
@end
