//
//  Agent.h
//  Invoice
//
//  Created by Aditi on 08/03/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Agent : NSObject
@property(nonatomic)NSString *strName;
@property(nonatomic)NSString *strEmail;
@property(nonatomic)NSString *strID;
- (id)initWithDictionary:(NSDictionary*)dict;
- (id)createAgent:(NSInteger)index;
@end
