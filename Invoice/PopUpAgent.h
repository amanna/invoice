//
//  PopUpAgent.h
//  Invoice
//
//  Created by Aditi on 20/03/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import <UIKit/UIKit.h>
enum EventTypeH {
    kActionSelect = 0,
};
typedef void (^EventCompletionHandlerH)(id object, NSUInteger EventTypeH);
@interface PopUpAgent : UIViewController{
    EventCompletionHandlerH eventCompletionHandlerH;
}
- (void)setEventOnCompletion:(EventCompletionHandlerH)handler;
@end
