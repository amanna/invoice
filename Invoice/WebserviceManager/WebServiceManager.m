//
//  WebServiceManager.m
//  WovaxUniversalApp
//
//  Created by Susim Samanta on 11/11/13.
//  Copyright (c) 2013 Wovax, LLC. All rights reserved.
//

#import "WebServiceManager.h"
#import "SSRestManager.h"
#import "AppConstants.h"
#import "Agent.h"
#import "Employee.h"
#import "Job.h"
@implementation WebServiceManager
+(void)loginOnCompletion:(NSString*)strUserName andPass:(NSString*)strPass onCompletion:(FetchCompletionHandler)handler{
    NSString *strLoginUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,kLogin];
strUserName = @"job2012.aditi@gmail.com";
 strPass= @"123456";
    //strUserName = @"rima@sketchwebsolutions.com";
    
     NSString *kQuery = [NSString stringWithFormat:@"email=%@&password=%@",strUserName,strPass];
    
       SSRestManager *restManager = [[SSRestManager alloc]init];
       [restManager getJsonResponseFromBaseUrl:strLoginUrl query:kQuery onCompletion:^(NSDictionary *json) {
           if(json){
               NSDictionary *res = [json valueForKey:@"result"];
               NSInteger status = [[res valueForKey:@"status"]integerValue];
               if(status==1){
                   NSString *strAdmin = [res valueForKey:@"is_admin"];
                   if([strAdmin isEqualToString:@"Y"]){
                       strAdmin = @"1";
                   }else{
                       strAdmin = @"0";
                   }
                    NSInteger id1 = [[res valueForKey:@"id"]integerValue];
                   NSString *strUid = [NSString stringWithFormat:@"%ld",(long)id1];
                    NSString *strUname = [res valueForKey:@"username"];
                    NSString *strEmail = [res valueForKey:@"email"];
                   NSString *strFname = [res valueForKey:@"firstname"];
                   
                   [[NSUserDefaults standardUserDefaults] setObject:strUid forKey:@"uid"];
                    [[NSUserDefaults standardUserDefaults] setObject:strUname forKey:@"uname"];
                   [[NSUserDefaults standardUserDefaults] setObject:strEmail forKey:@"email"];
                   [[NSUserDefaults standardUserDefaults] setObject:strAdmin forKey:@"isAdmin"];
                   [[NSUserDefaults standardUserDefaults] setObject:strFname forKey:@"fname"];
                   [[NSUserDefaults standardUserDefaults] synchronize];
                   handler(@"1",nil);
               }else{
                   handler(@"0",nil);
               }
           }
          } onError:^(NSError *error) {
              if (error) {
                  handler(nil,error);
              }
           
       }];

}
+(void)logoutOnCompletion:(NSString*)strUid onCompletion:(FetchCompletionHandler)handler{
    NSString *strLoginUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,kLogout];
    NSString *kQuery = [NSString stringWithFormat:@"id=%@",strUid];
    SSRestManager *restManager = [[SSRestManager alloc] init];
    [restManager getJsonResponseFromBaseUrl:strLoginUrl query:kQuery onCompletion:^(NSDictionary *json) {
        if (json) {
            BOOL strSucces = [[json valueForKey:@"Success"]boolValue];
            NSMutableArray *offerFullList = [[NSMutableArray alloc] init];
            if(strSucces == true){
                           }else{
                handler (offerFullList,nil);
            }
            
        }
    } onError:^(NSError *error) {
        if (error) {
            handler(nil,error);
        }
    }];

}
+(void)getAgentList:(NSString*)strUid onCompletion:(FetchCompletionHandler)handler{
    NSString *strLoginUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,kAgentList];
    
    NSString *kQuery = [NSString stringWithFormat:@"empid=%@",strUid];
    SSRestManager *restManager = [[SSRestManager alloc]init];
    [restManager getJsonResponseFromBaseUrl:strLoginUrl query:kQuery onCompletion:^(NSDictionary *json) {
        if(json){
            NSDictionary *res = [json valueForKey:@"result"];
            NSArray *arrAgent = [res valueForKey:@"agentlist"];
        NSMutableArray *arrAgentList = [[NSMutableArray alloc]init];
          [arrAgent enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL * _Nonnull stop) {
              Agent *agent = [[Agent alloc]initWithDictionary:dict];
              [arrAgentList addObject:agent];
              
          }];
//            for(int i=0;i<3;i++){
//                Agent *agent = [[Agent alloc]init];
//                agent= [agent createAgent:i];
//                [arrAgentList addObject:agent];
//            }
            handler (arrAgentList,nil);
        }
    } onError:^(NSError *error) {
        if (error) {
            handler(nil,error);
        }
        
    }];

}
+(void)getEmpListonCompletion:(FetchCompletionHandler)handler{
    NSString *strLoginUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,kEmpList];
    SSRestManager *restManager = [[SSRestManager alloc]init];
    [restManager getJsonResponseFromBaseUrl:strLoginUrl query:nil onCompletion:^(NSDictionary *json) {
        if(json){
           NSDictionary *res = [json valueForKey:@"result"];
            NSArray *arrUser = [res valueForKey:@"userdetails"];
            NSMutableArray *arruserList = [[NSMutableArray alloc]init];
            if(arrUser.count >0){
                [arrUser enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL * _Nonnull stop) {
                    Employee *agent = [[Employee alloc]initWithDictionary:dict];
                    [arruserList addObject:agent];
                    
                }];
                 handler(arruserList,nil);
            }else{
                 handler(arruserList,nil);
            }
        }
    } onError:^(NSError *error) {
        if (error) {
            handler(nil,error);
        }
        
    }];

}
+(void)agentCreation:(NSString*)strUid andEmpName:(NSString*)strEmpName andEmpMail:(NSString*)strEmail  onCompletion:(FetchCompletionHandler)handler{
    NSString *strLoginUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,kAgentcreate];
//    strEmpName = @"Agfg jdhdh";
//    strEmail = @"ads@gmail.com";
    NSString *kQuery = [NSString stringWithFormat:@"empid=%@&fullname=%@&email=%@",strUid,strEmpName,strEmail];
    SSRestManager *restManager = [[SSRestManager alloc]init];
    [restManager getJsonResponseFromBaseUrl:strLoginUrl query:kQuery onCompletion:^(NSDictionary *json) {
        if(json){
            NSDictionary *res = [json valueForKey:@"result"];
            NSInteger status = [[res valueForKey:@"status"]integerValue];
            
            if(status==1){
                handler (@"1",nil);
            }else{
                handler (@"0",nil);
            }
        }
    } onError:^(NSError *error) {
        if (error) {
            handler(nil,error);
        }
        
    }];

}
+(void)getInvoiceList:(NSString*)strEmp onCompletion:(FetchCompletionHandler)handler{
    NSString *strLoginUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,kInvoiceList];
    
    NSString *kQuery = [NSString stringWithFormat:@"empid=%@",strEmp];
    SSRestManager *restManager = [[SSRestManager alloc]init];
    [restManager getJsonResponseFromBaseUrl:strLoginUrl query:kQuery onCompletion:^(NSDictionary *json) {
        if(json){
            NSDictionary *res = [json valueForKey:@"result"];
            NSInteger status = [[res valueForKey:@"status"]integerValue];
            NSMutableArray *arrJob = [[NSMutableArray alloc]init];
            if(status==1){
                NSArray *invoiceArr = [res valueForKey:@"invoicelist"];
                
                [invoiceArr enumerateObjectsUsingBlock:^(NSDictionary *dictInvoice, NSUInteger idx, BOOL * _Nonnull stop) {
                    Job *job =[[Job alloc]initWithDictionary:dictInvoice];
                    [arrJob addObject:job];
                }];
                handler (arrJob,nil);
            }else{
                handler (arrJob,nil);
            }
        }
    } onError:^(NSError *error) {
        if (error) {
            handler(nil,error);
        }
        
    }];

}
+(void)createInvoice:(NSString*)strInvoice onCompletion:(FetchCompletionHandler)handler{
    NSString *strLoginUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,kInvoiceCreate];
    NSString *kQuery = [NSString stringWithFormat:@"%@",strInvoice];
    SSRestManager *restManager = [[SSRestManager alloc]init];
    [restManager getJsonResponseFromBaseUrl:strLoginUrl query:kQuery onCompletion:^(NSDictionary *json) {
        if(json){
            NSDictionary *res = [json valueForKey:@"result"];
            NSInteger status = [[res valueForKey:@"status"]integerValue];
            
            if(status==1){
                handler (@"1",nil);
            }else{
                handler (@"0",nil);
            }
        }
    } onError:^(NSError *error) {
        if (error) {
            handler(nil,error);
        }
        
    }];

}
+(void)addEmployee:(NSString*)strEmp onCompletion:(FetchCompletionHandler)handler{
    NSString *strLoginUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,kAddEmp];
    NSString *kQuery = [NSString stringWithFormat:@"%@",strEmp];
    SSRestManager *restManager = [[SSRestManager alloc]init];
    [restManager getJsonResponseFromBaseUrl:strLoginUrl query:kQuery onCompletion:^(NSDictionary *json) {
        if(json){
            NSDictionary *res = [json valueForKey:@"result"];
            NSInteger status = [[res valueForKey:@"status"]integerValue];
            NSString *strStatus = [NSString stringWithFormat:@"%ld",(long)status];
            handler (strStatus,nil);
            
        }
    } onError:^(NSError *error) {
        if (error) {
            handler(nil,error);
        }
        
    }];

}
+(void)EditEmployee:(NSString*)strEmp onCompletion:(FetchCompletionHandler)handler{
    NSString *strLoginUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,kEditEmp];
    NSString *kQuery = [NSString stringWithFormat:@"%@",strEmp];
    SSRestManager *restManager = [[SSRestManager alloc]init];
    [restManager getJsonResponseFromBaseUrl:strLoginUrl query:kQuery onCompletion:^(NSDictionary *json) {
        if(json){
            NSDictionary *res = [json valueForKey:@"result"];
            NSInteger status = [[res valueForKey:@"status"]integerValue];
            
            if(status==1){
                handler (@"1",nil);
            }else{
                handler (@"0",nil);
            }
        }
    } onError:^(NSError *error) {
        if (error) {
            handler(nil,error);
        }
        
    }];

}
+(void)deleteEmployee:(NSString*)strEmp onCompletion:(FetchCompletionHandler1)handler{
    NSString *strLoginUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,kDeleteEmp];
    NSString *kQuery = [NSString stringWithFormat:@"userid=%@",strEmp];
    SSRestManager *restManager = [[SSRestManager alloc]init];
    [restManager getJsonResponseFromBaseUrl:strLoginUrl query:kQuery onCompletion:^(NSDictionary *json) {
        if(json){
            NSDictionary *res = [json valueForKey:@"result"];
            NSInteger status = [[res valueForKey:@"status"]integerValue];
            
            if(status==1){
                
                NSArray *arrUser = [res valueForKey:@"userdetails"];
                NSMutableArray *arruserList = [[NSMutableArray alloc]init];
                if(arrUser.count >0){
                    [arrUser enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL * _Nonnull stop) {
                        Employee *agent = [[Employee alloc]initWithDictionary:dict];
                        [arruserList addObject:agent];
                        
                    }];
                   
                    handler(@"1",arruserList,nil);
                }else{
                     handler(@"1",arruserList,nil);
                }
                
                
                //handler (@"1",nil);
            }else{
                NSString *strMsg = [res valueForKey:@"message"];
                handler(@"0",strMsg,nil);
            }
        }
    } onError:^(NSError *error) {
        if (error) {
             handler(nil,nil,error);
        }
        
    }];

}
+(void)deleteAgent:(NSString*)strEmp onCompletion:(FetchCompletionHandler)handler{
    NSString *strLoginUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,kDeleteAgent];
    NSString *kQuery = [NSString stringWithFormat:@"agentid=%@",strEmp];
    SSRestManager *restManager = [[SSRestManager alloc]init];
    [restManager getJsonResponseFromBaseUrl:strLoginUrl query:kQuery onCompletion:^(NSDictionary *json) {
        if(json){
            NSDictionary *res = [json valueForKey:@"result"];
            NSInteger status = [[res valueForKey:@"status"]integerValue];
            
            if(status==1){
                handler (@"1",nil);
            }else{
                handler (@"0",nil);
            }
        }
    } onError:^(NSError *error) {
        if (error) {
            handler(nil,error);
        }
        
    }];

}
+(void)sendExcel:(NSString*)strEmp onCompletion:(FetchCompletionHandler)handler{
    NSString *strLoginUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,kSendExcel];
    NSString *kQuery = [NSString stringWithFormat:@"empid=%@",strEmp];
    SSRestManager *restManager = [[SSRestManager alloc]init];
    [restManager getJsonResponseFromBaseUrl:strLoginUrl query:kQuery onCompletion:^(NSDictionary *json) {
        if(json){
            NSDictionary *res = [json valueForKey:@"result"];
            NSInteger status = [[res valueForKey:@"status"]integerValue];
            
            if(status==1){
                handler (@"1",nil);
            }else{
                handler (@"0",nil);
            }
        }
    } onError:^(NSError *error) {
        if (error) {
            handler(nil,error);
        }
        
    }];

}
+(void)EditAgent:(NSString*)strEmp onCompletion:(FetchCompletionHandler)handler{
  NSString *strLoginUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,kEditAgent];
  SSRestManager *restManager = [[SSRestManager alloc]init];
  [restManager getJsonResponseFromBaseUrl:strLoginUrl query:strEmp onCompletion:^(NSDictionary *json) {
      if(json){
          NSDictionary *res = [json valueForKey:@"result"];
          NSInteger status = [[res valueForKey:@"status"]integerValue];
          
          if(status==1){
              handler (@"1",nil);
          }else{
              handler (@"0",nil);
          }
      }
  } onError:^(NSError *error) {
      if (error) {
          handler(nil,error);
      }
      
  }];

}
+(void)deleteInvoice:(NSString*)strInvoice onCompletion:(FetchCompletionHandler)handler{
    
    NSString *strLoginUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,kDeleteInvoice];
    NSString *kQuery = [NSString stringWithFormat:@"invoiceid=%@",strInvoice];
    SSRestManager *restManager = [[SSRestManager alloc]init];
    [restManager getJsonResponseFromBaseUrl:strLoginUrl query:kQuery onCompletion:^(NSDictionary *json) {
        if(json){
            NSDictionary *res = [json valueForKey:@"result"];
            NSInteger status = [[res valueForKey:@"status"]integerValue];
            
            if(status==1){
                handler (@"1",nil);
            }else{
                handler (@"0",nil);
            }
        }
    } onError:^(NSError *error) {
        if (error) {
            handler(nil,error);
        }
        
    }];

}
+(void)getSettingsInfo:(FetchCompletionHandler)handler{
    
    
   
   
}

//offer list
//{"Success":true,"msg":"Offer Found","OfferList":[{"OfferId":11,"OfferName":"Free Play Cash!","OfferDescription":"Free Play Cash","OfferCode":"FPC","OfferValidityPeriod":"","OfferExpDate":"1\/15\/2017 12:00:00 AM","OfferImage":"https:\/\/landingpage.experiture.com\/TrackingSystem\/AG_44625\/AD_44626\/Images\/all_off_1.jpg"}]}
//{"Success":true,"msg":"Login successfully","User_Details":{"Name":"Chandrahas","TargetId":"2022","UserPoints":5647,"Tire":"Platinum"}}
//+(void)fetchWovaxMenusOnCompletion:(FetchCompletionHandler)handler{
//    SSRestManager *restManager = [[SSRestManager alloc] init];
//    NSString *menuUrl = [NSString stringWithFormat:@"%@%@",kApiKey,kMenuURL];
//    [restManager getJsonResponseFromBaseUrl:kBaseURL query:menuUrl onCompletion:^(NSDictionary *json) {
//        if (json) {
//                NSArray *menuList = [json valueForKey:@"menu"];
//                NSMutableArray *menuFullList = [[NSMutableArray alloc] init];
//                [menuList enumerateObjectsUsingBlock:^(NSDictionary *menuDict, NSUInteger idx, BOOL *stop) {
//                    Menu *menu=[[Menu alloc]initWithDictionary:menuDict];
//                    [menuFullList addObject:menu];
//                    
//                }];
//                
//            handler (menuFullList,nil);
//        }
//    } onError:^(NSError *error) {
//        if (error) {
//            handler(nil,error);
//        }
//    }];
//}
//+ (void)fetchImageDataWithLink:(NSString *)imageLink OnCompletion:(FetchCompletionHandler)handler {
//    SSRestManager *restManager = [[SSRestManager alloc] init];
//    [restManager getServiceResponseWithBaseUrl:imageLink query:nil onCompletion:^(id responseData, NSURLResponse *reponse) {
//        handler (responseData,nil);
//    } onError:^(NSError *error) {
//        handler (nil,error);
//    }];
//
//}
//+ (void)fetchRecentPostOnCompletion:(FetchCompletionHandler)handler
//{
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        NSError *jsonError;
//        if (!jsonError) {
//            dispatch_async(dispatch_get_main_queue(), ^(void){
//                SSRestManager *restManager = [[SSRestManager alloc] init];
//                NSString *strPost = [NSString stringWithFormat:@"%@%@",kApiKey,kPostURL];
//                [restManager getJsonResponseFromBaseUrl:kBaseURL query:strPost onCompletion:^(NSDictionary *json) {
//                    if (json) {
//                        NSArray *arrPost = [json valueForKey:@"posts"];
//                        NSMutableArray *postList = [[NSMutableArray alloc] init];
//                        [arrPost enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
//                            Post *recent = [[Post alloc]initWithDictionary:dict];
//                            if(![recent.postHidden isEqualToString:@"1"] && ![recent.postCatId isEqualToString:@"3"]&& ![recent.postCatId isEqualToString:@"5"]){
//                                [postList addObject:recent];
//                            }
//                            
//                        }];
//                        handler (postList,nil);
//                    }
//                } onError:^(NSError *error) {
//                    if (error) {
//                        handler(nil,error);
//                    }
//                }];
//
//                
//                
//                
//            });
//        }
//    });
//
//    
//    
//}
//
//+ (void)fetchPostDetails:(NSString *)strPostId onCompletion:(FetchCompletionHandler)handler
//{
//    NSString *strPostDetails = [NSString stringWithFormat:@"%@%@",kApiKey,kPostDetails];
//    NSString *kQuery = [strPostDetails stringByAppendingString:strPostId];
//    SSRestManager *restManager = [[SSRestManager alloc]init];
//    [restManager getJsonResponseFromBaseUrl:kBaseURL query:kQuery onCompletion:^(NSDictionary *json) {
//        if(json){
//            NSDictionary *dictPost = [json valueForKey:@"post"];
//           Post *recent = [[Post alloc]initWithDictionary:dictPost];
//            handler (recent,nil);
//          }
//       } onError:^(NSError *error) {
//           if (error) {
//               handler(nil,error);
//           }
//        
//    }];
//}
//
//+ (void)submitComment:(NSString *)strPostId  withText:(NSString*)strComment  onCompletion:(FetchCompletionHandler)handler {
//    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
//    NSString *name = [prefs stringForKey:@"Name"];
//    name = [name stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
//    NSString *email = [prefs stringForKey:@"Email"];
//    NSString *website = [prefs stringForKey:@"Website"];
//    strComment = [self URLEncodeStringFromString:strComment];
//    NSString *strPost = [NSString stringWithFormat:@"post_id=%@&name=%@&url=%@&email=%@&content=%@", strPostId,name,website,email,strComment];
//    NSString *kQuery = [NSString stringWithFormat:@"%@%@%@%@",kBaseURL,kApiKey,kComment,strPost];
//    SSRestManager *restManager = [[SSRestManager alloc]init];
//    [restManager getJsonResponseFromBaseUrl:kQuery query:@"" onCompletion:^(NSDictionary *json) {
//        if(json){
//           handler (@"",nil);
//        }
//    } onError:^(NSError *error) {
//        if (error) {
//            handler(nil,error);
//        }
//    }];
//}
//+ (NSString *)URLEncodeStringFromString:(NSString *)string {
//    static CFStringRef charset = CFSTR("!@#$%&*()+'\";:=,/?[] ");
//    CFStringRef str = (__bridge CFStringRef)string;
//    CFStringEncoding encoding = kCFStringEncodingUTF8;
//    return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL, str, NULL, charset, encoding));
//}
//+ (void)fetchMenuDetails:(NSString*)strMenuId withPage:(NSInteger)page onCompletion:(FetchCompletionHandler)handler{
//    NSString *kQuery = [NSString stringWithFormat:@"%@%@%@%@&page=%ld",kBaseURL,kApiKey,kMenuDetails,strMenuId,(long)page];
//    SSRestManager *restmanger = [[SSRestManager alloc]init];
//    [restmanger getJsonResponseFromBaseUrl:kQuery query:kQuery onCompletion:^(NSDictionary *json) {
//        if(json){
//            NSArray *arrPost = [json valueForKey:@"posts"];
//            NSMutableArray *postList = [[NSMutableArray alloc] init];
//            [arrPost enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
//                Post *recent = [[Post alloc]initWithDictionary:dict];
//                [postList addObject:recent];
//            }];
//            handler (postList,nil);
//        }
//    } onError:^(NSError *error) {
//        handler(nil,error);
//    }];
//    
//}
//+ (void)fetchtagMenuDetails:(NSString*)strMenuId withPage:(NSInteger)page onCompletion:(FetchCompletionHandler)handler{
//    NSString *kQuery = [NSString stringWithFormat:@"%@%@%@%@&page=%ld",kBaseURL,kApiKey,kTagDetails,strMenuId,(long)page];
//    SSRestManager *restmanger = [[SSRestManager alloc]init];
//    [restmanger getJsonResponseFromBaseUrl:kQuery query:kQuery onCompletion:^(NSDictionary *json) {
//        if(json){
//            NSArray *arrPost = [json valueForKey:@"posts"];
//            NSMutableArray *postList = [[NSMutableArray alloc] init];
//            [arrPost enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
//                Post *recent = [[Post alloc]initWithDictionary:dict];
//                [postList addObject:recent];
//            }];
//            handler (postList,nil);
//        }
//    } onError:^(NSError *error) {
//        handler(nil,error);
//    }];
//
//}
//+ (void)fetchSearchDetails:(NSString *)strSearch onCompletiion:(FetchCompletionHandler)handler
//{
//    strSearch = [strSearch stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
//    NSString *kQuery = [NSString stringWithFormat:@"%@%@%@",kApiKey,kSearch,strSearch];
//    SSRestManager *restmanger = [[SSRestManager alloc]init];
//    [restmanger getJsonResponseFromBaseUrl:kBaseURL query:kQuery onCompletion:^(NSDictionary *json) {
//        if(json){
//            NSArray *arrPost = [json valueForKey:@"posts"];
//            NSMutableArray *postList = [[NSMutableArray alloc] init];
//            [arrPost enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
//                Post *recent = [[Post alloc]initWithDictionary:dict];
//                [postList addObject:recent];
//            }];
//            handler (postList,nil);
//        }
//    } onError:^(NSError *error) {
//        handler(nil,error);
//    }];
//}
//+ (void)fetchPostTypeDetails:(NSString *)strPostId withType:(NSString*)strType onCompletion:(FetchCompletionHandler)handler {
//    NSString *kQuery = [NSString stringWithFormat:@"%@%@%@post_id=%@&post_type=%@",kBaseURL,kApiKey,kPostDetailsType,strPostId,strType];
//    SSRestManager *restManager = [[SSRestManager alloc]init];
//    [restManager getJsonResponseFromBaseUrl:kQuery query:kQuery onCompletion:^(NSDictionary *json) {
//        if(json){
//            NSDictionary *dictPost = [json valueForKey:@"post"];
//            Post *recent = [[Post alloc]initWithDictionary:dictPost];
//            handler (recent,nil);
//        }
//    } onError:^(NSError *error) {
//        if (error) {
//            handler(nil,error);
//        }
//    }];
//}
//
//+ (void)fetchRecentPostPagewiseOnCompletion:(NSInteger)page onCompletion:(FetchCompletionHandler)handler {
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        NSString *urlString = [NSString stringWithFormat:@"%@%@%@",kBaseURL ,kApiKey,kOptions];
//        NSData *trackerData = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
//        NSError *jsonError;
//        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:trackerData options:kNilOptions error:&jsonError];
//        if (!jsonError) {
//            dispatch_async(dispatch_get_main_queue(), ^(void){
//                NSString *excludepost = [json objectForKey:@"wovax_excluded_categories"];
//                NSArray *arr;
//                if(![excludepost isEqualToString:@""]){
//                    NSString *leftString = [excludepost substringFromIndex:1];
//                    arr = [leftString componentsSeparatedByString:@","];
//                }
//                SSRestManager *restManager = [[SSRestManager alloc] init];
//                NSString *strPage = [NSString stringWithFormat:@"%@%@%ld",kApiKey,kPostURLPage,(long)page];
//                [restManager getJsonResponseFromBaseUrl:kBaseURL query:strPage onCompletion:^(NSDictionary *json) {
//                    if (json) {
//                        NSArray *arrPost = [json valueForKey:@"posts"];
//                        NSMutableArray *postList = [[NSMutableArray alloc] init];
//                        [arrPost enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
//                            Post *recent = [[Post alloc]initWithDictionary:dict];
//                            if(arr.count > 0){
//                                if(![recent.postHidden isEqualToString:@"1"] && ![arr containsObject:recent.postCatId]){
//                                    [postList addObject:recent];
//                                }
//                            }else{
//                                if(![recent.postHidden isEqualToString:@"1"]){
//                                    [postList addObject:recent];
//                                }
//                            }
//                            
//                        }];
//                        handler (postList,nil);
//                        
//                    }
//                } onError:^(NSError *error) {
//                    if (error) {
//                        handler(nil,error);
//                    }
//                }];
//            });
//        }else{
//            NSLog(@"%@",jsonError.debugDescription);
//        }
//    });
//    
//}
//+ (void)callClearBadgeTokenServiceOncompletion:(FetchCompletionHandler)handler {
//    SSRestManager *restManager = [[SSRestManager alloc] init];
//    NSString *menuUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,kClearBadgeToken];
//    [restManager getJsonResponseFromBaseUrl:kBaseURL query:menuUrl onCompletion:^(NSDictionary *json) {
//        if (json) {
//            handler (nil,nil);
//        }
//    } onError:^(NSError *error) {
//        if (error) {
//            handler(nil,error);
//        }
//    }];
//}
//+ (void)callMapService:(FetchCompletionHandler)handler{
//    SSRestManager *restManager = [[SSRestManager alloc] init];
//    NSString *urlString = [NSString stringWithFormat:@"%@%@%@",kBaseURL,kApiKey,kMap];
//    [restManager getJsonResponseFromBaseUrl:kBaseURL query:urlString onCompletion:^(NSDictionary *json) {
//        if (json) {
//            NSString *satuts = [json valueForKey:@"status"];
//            if([satuts isEqualToString:@"ok"]){
//                 NSArray *arrPost = [json valueForKey:@"posts"];
//                 NSMutableArray *arrMap = [[NSMutableArray alloc]init];
//                 [arrPost enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
//                     Post *recent = [[Post alloc]init];
//                     recent.postTitle = [dict valueForKey:@"title"];
//                     recent.postLongitude = dict[@"custom_fields"][@"geo_longitude"];
//                     recent.postLatt = dict[@"custom_fields"][@"geo_latitude"];
//                     recent.postId = [dict valueForKey:@"id"];
//                     [arrMap addObject:recent];
//                 }];
//                 handler (arrMap,nil);
//            }
//            
//        }
//    } onError:^(NSError *error) {
//        if (error) {
//            handler(nil,error);
//        }
//    }];
//
//}
//+ (void)storeDeviceMeataDataToBackendOnCompletion:(FetchCompletionHandler)handler {
//    SSRestManager *restManager = [[SSRestManager alloc] init];
//    NSString *vendorID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
//    NSString *systemName = [[UIDevice currentDevice] systemName];
//    NSString *systemModel = [[UIDevice currentDevice] model];
//    NSString *menuUrl = [NSString stringWithFormat:@"/?wovaxmenu=downloads&userID=%@&OS=%@&device=%@",vendorID,systemName,systemModel];
//    NSString *finalURL  = [menuUrl stringByReplacingOccurrencesOfString:@" " withString:@"%20"]
//    ;    [restManager getJsonResponseFromBaseUrl:kBaseURL query:finalURL onCompletion:^(NSDictionary *json) {
//        if (json) {
//            handler (nil,nil);
//        }
//    } onError:^(NSError *error) {
//        if (error) {
//            handler(nil,error);
//        }
//    }];
//}
//+ (void)getAppSettingsDataOnCompletion:(FetchCompletionHandler)handler {
//    SSRestManager *restManager = [[SSRestManager alloc] init];
//    NSString *urlString = [NSString stringWithFormat:@"/%@%@" ,kApiKey,kOptions2];
//    [restManager getJsonResponseFromBaseUrl:kBaseURL query:urlString onCompletion:^(NSDictionary *json) {
//        if (json) {
//            if([[json valueForKey:@"status"] isEqualToString:@"ok"]){
//                AppSettings *appSettings = [[AppSettings alloc] initWithDict:json];
//                handler(appSettings,nil);
//            }
//        }
//    } onError:^(NSError *error) {
//        if (error) {
//            handler(nil,error);
//        }
//    }];
//}
//+ (void)getWovaxSettingsDataOnCompletion:(FetchCompletionHandler)handler {
//    SSRestManager *restManager = [[SSRestManager alloc] init];
//     NSString *urlString = [NSString stringWithFormat:@"/%@%@" ,kApiKey,kOptions];
//    [restManager getJsonResponseFromBaseUrl:kBaseURL query:urlString onCompletion:^(NSDictionary *json) {
//        if (json) {
//            WovaxSetting *wovaxSettings = [[WovaxSetting alloc] initWithDict:json];
//            handler(wovaxSettings,nil);
//        }
//    } onError:^(NSError *error) {
//        if (error) {
//            handler(nil,error);
//        }
//    }];
//}
//+ (void)fetchRecentPostDictOfPage:(NSInteger)page onCompletion:(FetchCompletionHandler)handler {
//    /*NSString *query = [NSString stringWithFormat:@"%@%@%ld",kApiKey,kPostURLPage,(long)page];
//    NSString *url = [NSString stringWithFormat:@"%@%@",kBaseURL,query];
//    NSDictionary *headerDict;
//    SSHTTPClient *client = [[SSHTTPClient alloc] initWithUrl:url method:@"GET" httpBody:@"" headerFieldsAndValues:headerDict];
//    [client getJsonData:^(id jsonData, NSError *error) {
//        if (error) {
//            handler(nil,error);
//        }else if (jsonData) {
//            handler(jsonData,nil);
//        }else {
//            handler(nil,nil);
//        }
//    }];*/
//    
//    SSRestManager *restManager = [[SSRestManager alloc] init];
//    NSString *strPage = [NSString stringWithFormat:@"%@%@%ld",kApiKey,kPostURLPage,(long)page];
//    [restManager getJsonResponseFromBaseUrl:kBaseURL query:strPage onCompletion:^(NSDictionary *json) {
//        if (json) {
//            handler (json,nil);
//        }
//    } onError:^(NSError *error) {
//        if (error) {
//            handler(nil,error);
//        }
//    }];
//}
//#pragma mark Notification Service
//+ (void)fetchNotificationOfDeviceId:(NSString *)deviceId OnCompletion:(FetchCompletionHandler)handler {
//    SSRestManager *restManager = [[SSRestManager alloc] init];
//    NSString *strPage = [NSString stringWithFormat:@"%@%@?deviceId=%@&platform=apns",kApiKey,kNotificationURL,deviceId];
//    [restManager getJsonResponseFromBaseUrl:kBaseURL query:strPage onCompletion:^(NSDictionary *json) {
//        if (json) {
//            NSArray *categories = json[@"categories"];
//            NSMutableArray *notiifcationList = [[NSMutableArray alloc] init];
//            [categories enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
//                Notification *notif = [[Notification alloc] initWithDict:obj];
//                [notiifcationList addObject:notif];
//            }];
//            handler (notiifcationList,nil);
//        }
//    } onError:^(NSError *error) {
//        if (error) {
//            handler(nil,error);
//        }
//    }];
//}
//#pragma mark Push notification
//+ (void)registerRemoteNotificationWithDeviceId:(NSString *)deviceToken onCompletion:(FetchCompletionHandler)handler {
//    NSString *kQuery = [NSString stringWithFormat:@"%@%@%@",kApiKey,kRegisterAPNS,deviceToken];
//    SSRestManager *restmanger = [[SSRestManager alloc]init];
//    [restmanger getJsonResponseFromBaseUrl:kBaseURL query:kQuery onCompletion:^(NSDictionary *json) {
//        if(json){
//            handler (json,nil);
//        }
//    } onError:^(NSError *error) {
//        handler(nil,error);
//    }];
//}
//+ (void)unsubscribePushNotificationOnCatergories:(NSString *)categories deviceId:(NSString *)deviceId onCompletion:(FetchCompletionHandler)handler {
//    NSString *kQuery = [NSString stringWithFormat:@"%@%@%@&categories=%@",kApiKey,kUnsubscribeAPNS,deviceId,categories];
//    SSRestManager *restmanger = [[SSRestManager alloc]init];
//    [restmanger getJsonResponseFromBaseUrl:kBaseURL query:kQuery onCompletion:^(NSDictionary *json) {
//        if(json){
//            handler (json,nil);
//        }
//    } onError:^(NSError *error) {
//        handler(nil,error);
//    }];
//}
@end
