//
//  WebServiceManager.h
//  WovaxUniversalApp
//
//  Created by Susim Samanta on 11/11/13.
//  Copyright (c) 2013 Wovax, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^FetchCompletionHandler)(id object, NSError *error);
typedef void (^FetchCompletionHandler1)(id object,id object1, NSError *error);
@interface WebServiceManager : NSObject
+(void)loginOnCompletion:(NSString*)strUserName andPass:(NSString*)strPass onCompletion:(FetchCompletionHandler)handler;
+(void)logoutOnCompletion:(NSString*)strUid onCompletion:(FetchCompletionHandler)handler;
+(void)agentCreation:(NSString*)strUid andEmpName:(NSString*)strEmpName andEmpMail:(NSString*)strEmail  onCompletion:(FetchCompletionHandler)handler;
+(void)getAgentList:(NSString*)strUid onCompletion:(FetchCompletionHandler)handler;
+(void)createInvoice:(NSString*)strInvoice onCompletion:(FetchCompletionHandler)handler;
+(void)getInvoiceList:(NSString*)strEmp onCompletion:(FetchCompletionHandler)handler;


+(void)addEmployee:(NSString*)strEmp onCompletion:(FetchCompletionHandler)handler;
+(void)EditEmployee:(NSString*)strEmp onCompletion:(FetchCompletionHandler)handler;
+(void)deleteEmployee:(NSString*)strEmp onCompletion:(FetchCompletionHandler1)handler;
+(void)EditAgent:(NSString*)strEmp onCompletion:(FetchCompletionHandler)handler;
+(void)deleteAgent:(NSString*)strEmp onCompletion:(FetchCompletionHandler)handler;
+(void)getEmpListonCompletion:(FetchCompletionHandler)handler;
+(void)sendExcel:(NSString*)strEmp onCompletion:(FetchCompletionHandler)handler;
+(void)deleteInvoice:(NSString*)strInvoice onCompletion:(FetchCompletionHandler)handler;
+(void)getSettingsInfo:(FetchCompletionHandler)handler;
@end

//{"Success":true,"msg":"Login successfully","User_Details":{"Name":"Chandrahas","TargetId":"2022","UserPoints":5647,"Tire":"Platinum"}}

//offer list
//{"Success":true,"msg":"Offer Found","OfferList":[{"OfferId":11,"OfferName":"Free Play Cash!","OfferDescription":"Free Play Cash","OfferCode":"FPC","OfferValidityPeriod":"","OfferExpDate":"1\/15\/2017 12:00:00 AM","OfferImage":"https:\/\/landingpage.experiture.com\/TrackingSystem\/AG_44625\/AD_44626\/Images\/all_off_1.jpg"}]}


//http://api.experiture.com/rest/v1/json/getOfferDetailListRest/27CCC3A558E542DD827649EED27F64C3/22.4696554/88.393355/2023/0
//http://api.experiture.com/rest/v1/json/getOfferDetailListRest/{apikey}/{Latitude}/{Longitude}/{Targetid}/{OfferId}
//
//http://api.experiture.com/rest/v1/json/getMyOfferListRest/27CCC3A558E542DD827649EED27F64C3/22.4696554/88.393355/2023/0
//http://api.experiture.com/rest/v1/json/getMyOfferListRest/{apikey}/{Latitude}/{Longitude}/{Targetid}/{OfferId}
//
//http://api.experiture.com/rest/v1/json/saveMyOfferRest/27CCC3A558E542DD827649EED27F64C3/2023/11
//http://api.experiture.com/rest/v1/json/saveMyOfferRest/{apikey}/{Targetid}/{OfferId}
