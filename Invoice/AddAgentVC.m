//
//  AddAgentVC.m
//  Invoice
//
//  Created by Aditi on 19/03/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "AddAgentVC.h"
#import "POAcvityView.h"
#import "WebServiceManager.h"
@interface AddAgentVC (){
    POAcvityView *activity;
}
- (IBAction)btnCancelAction:(UIButton *)sender;
- (IBAction)btnSubmitAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end

@implementation AddAgentVC
//The event handling method
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    [self.view endEditing:true];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [singleFingerTap setCancelsTouchesInView:NO];
    [self.view addGestureRecognizer:singleFingerTap];
     activity = [[POAcvityView alloc]initWithTitle:@"Loading.." message:@"Loading.."];
    if(self.isEdit==YES){
        self.txtUserNmae.text = self.agent.strName;
        self.txtPass.text = self.agent.strEmail;
        self.lblTitle.text = @"Edit Agent";
        
    }else{
        self.lblTitle.text = @"Add Agent";
    }

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    if(textField==self.txtPass){
        NSString *emailReg = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailReg];
        
        if ([emailTest evaluateWithObject:textField.text] == NO)
        {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Enter valid Email" message:@"Please Enter Valid Email Address." delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
            [alert show];
            
        }
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField==self.txtUserNmae){
        [self.txtUserNmae resignFirstResponder];
        [self.txtPass becomeFirstResponder];

    }else{
        [self.txtPass resignFirstResponder];
    }
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnCancelAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)clearAllField{
    
}
- (IBAction)btnSubmitAction:(UIButton *)sender {
    
    if([self.txtUserNmae.text isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Create Agent"
                                                        message:@"Please enter Agent Name"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];

    }else if ([self.txtPass.text isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Create Agent"
                                                        message:@"Please enter Agent Email"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }else{
        if(self.isEdit==YES){
            [activity showView];
            //agentid,empid,fullname,email
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSString *strUid = [prefs stringForKey:@"uid"];
            
            NSString *kQuery = [NSString stringWithFormat:@"empid=%@&agentid=%@&fullname=%@&email=%@",strUid,self.agent.strID,self.txtUserNmae.text,self.txtPass.text];
            [WebServiceManager EditAgent:kQuery onCompletion:^(id object, NSError *error) {
                [activity hideView];
                if(object){
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Update Agent"
                                                                    message:@"Agent updated successfully"
                                                                   delegate:self
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    alert.tag = 99;
                    [alert show];
                }else{
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Update Agent"
                                                                    message:@"Some Problem Occurs!!!Please try again later"
                                                                   delegate:self
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    [alert show];

                }
            }];
            
        }else{
            [activity showView];
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSString *strUid = [prefs stringForKey:@"uid"];
            [WebServiceManager agentCreation:strUid andEmpName:self.txtUserNmae.text andEmpMail:self.txtPass.text onCompletion:^(id object, NSError *error) {
                [activity hideView];
                if(object){
                    NSString *str = object;
                    if([str isEqualToString:@"1"]){
                        [self clearAllField];
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Create Agent"
                                                                        message:@"Agent added successfully"
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                        alert.tag=100;
                        
                        [alert show];
                    }else{
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Create Agent"
                                                                        message:@"Some Problem Occurs!!!Please try again later"
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                        [alert show];
                    }
                }else{
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Create Agent"
                                                                    message:@"Some Problem Occurs!!!Please try again later"
                                                                   delegate:self
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    [alert show];
                }
            }];
            

        }
    }
    
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 100 || alertView.tag == 99)
    {
        if (buttonIndex == 0)
        {
           // [self performSegueWithIdentifier:@"segueAgentList" sender:self];
             [self.navigationController popViewControllerAnimated:YES];
        }
    }
}
@end
