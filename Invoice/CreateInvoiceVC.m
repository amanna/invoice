//
//  CreateInvoiceVC.m
//  Invoice
//
//  Created by MAPPS MAC on 24/02/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "CreateInvoiceVC.h"
#import "InvoiceHeaderCell.h"
#import "InvoiceEvidence.h"
#import "InvoiceADD.h"
#import "InvoiceSpreadTextCell.h"
#import "WebServiceManager.h"
#import "POAcvityView.h"
#import "AppDelegate.h"
#import "PopUpAgent.h"
#import "Agent.h"
#import "SignatureViewQuartzQuadratic.h"
#import "AppConstants.h"
@interface CreateInvoiceVC ()<UIPopoverControllerDelegate,UITextFieldDelegate>{
    POAcvityView *activity;
    AppDelegate *delegate;
    InvoiceEvidence *cellE;
}
@property (weak, nonatomic) IBOutlet UITextField *txtInvoiceNo;

@property (weak, nonatomic) IBOutlet SignatureViewQuartzQuadratic *signatureView;

- (IBAction)btnClearAction:(UIButton *)sender;
@property(nonatomic)UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UICollectionView *myCollection;
- (IBAction)btnAgentAction:(UIButton *)sender;
@property(nonatomic) NSMutableArray *arrHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblAgent;
- (IBAction)btnBackAction:(UIButton *)sender;
@property(nonatomic)NSInteger noOfColumn;
@property(nonatomic)PopUpAgent *popUp;
@property(nonatomic)NSInteger maxTag;
@property(nonatomic)NSInteger lastTag;
@property(nonatomic)NSString *strWebservice;
#define  tagText 9000
@property (weak, nonatomic) IBOutlet UILabel *lblJobDt;
- (IBAction)btnJobDtAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UITextField *textCustomerRef;
- (IBAction)btnDateAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnDate;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UITextField *txtEmailTo;
- (IBAction)btnConfirmAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet SignatureViewQuartzQuadratic *myDigitalView;
@property(nonatomic)NSMutableDictionary *dict;
@property(nonatomic)NSInteger imgTagCamera;
@property (weak, nonatomic) IBOutlet UIView *viewDt;

#define  tagDelete 3000
@property(nonatomic)NSString *strSubTotal;
@property(nonatomic)NSString *strVat;
@property(nonatomic)NSString *strTotal;
@property (weak, nonatomic) IBOutlet UILabel *lblSubTot;
@property (weak, nonatomic) IBOutlet UILabel *lblVat;
@property (weak, nonatomic) IBOutlet UILabel *lblTot;
@property (weak, nonatomic) IBOutlet UISwitch *vatswitch;

- (IBAction)switchChange:(UISwitch *)sender;

- (IBAction)btnEraseAction:(UIButton *)sender;

@end

@implementation CreateInvoiceVC
- (void)reloadTotal{
    float subTot=0;
    float vat;
    float total;
    for (NSInteger a=9001; a< 9040; a++) {
        NSInteger sec = (a - 9001)/5;
        NSInteger row =  (a - 9001)%5;
        
        if(row==3){
            NSString *strTag = [NSString stringWithFormat:@"%ld",(long)a];
            subTot= subTot + [[self.dict valueForKey:strTag]floatValue];
        }
    }
    vat = (subTot *20)/100;
    total= vat + subTot;
    
    self.strSubTotal = [NSString stringWithFormat:@"%0.2f",subTot];
    self.strVat = [NSString stringWithFormat:@"%0.2f",vat];
    self.strTotal = [NSString stringWithFormat:@"%0.2f",total];
    
    self.lblSubTot.text = self.strSubTotal;
    self.lblVat.text = self.strVat;
    
    self.lblTot.text = self.strTotal;
    
}
- (IBAction)switchChange:(UISwitch *)sender {
    if([sender isOn]){
         float vat = ([self.strSubTotal floatValue] * 20)/100;
         self.strVat = [NSString stringWithFormat:@"%0.2f",vat];
        self.lblVat.text = self.strVat;
        float sub = [self.strSubTotal floatValue];
        float tot = sub + vat;
         self.strTotal = [NSString stringWithFormat:@"%0.2f",tot];
        self.lblTot.text = self.strTotal;
    } else{
        float vat = 0.00;
        self.strVat = [NSString stringWithFormat:@"%0.2f",vat];
        self.lblVat.text = self.strVat;
        float sub = [self.strSubTotal floatValue];
        float tot = sub + vat;
         self.strTotal = [NSString stringWithFormat:@"%0.2f",tot];
        self.lblTot.text = self.strTotal;
    }
    
   
    
}

- (IBAction)btnEraseAction:(UIButton *)sender {
     [self.signatureView erase];
}
//The event handling method
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    [self.view endEditing:true];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    //The setup code (in viewDidLoad in your view controller)
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [singleFingerTap setCancelsTouchesInView:NO];
    [self.view addGestureRecognizer:singleFingerTap];
    
    self.dict = [[NSMutableDictionary alloc]init];
    self.noOfColumn = 3;
    self.lastTag = tagText;
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main"
                                                         bundle:nil];
    self.popUp =
    [storyboard instantiateViewControllerWithIdentifier:@"PopUpAgent"];
    
    [self.popUp setEventOnCompletion:^(id object, NSUInteger EventTypeH) {
        if(EventTypeH==kActionSelect){
            Agent *agent = object;
            self.strAgent = agent.strID;
            self.lblAgent.text = agent.strName;
            [self.popUp.view removeFromSuperview];
        }
    }];

    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    // you can now all you application delegat's properties.
     activity = [[POAcvityView alloc]initWithTitle:@"Loading.." message:@"Loading.."];
    //[activity showView];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *strUid = [prefs stringForKey:@"uid"];
    

    [self.myCollection registerClass:[InvoiceHeaderCell class] forCellWithReuseIdentifier:@"CellH"];
    [self.myCollection registerNib:[UINib nibWithNibName:@"InvoiceHeaderCell" bundle:nil] forCellWithReuseIdentifier:@"CellH"];
    
    [self.myCollection registerClass:[InvoiceSpreadTextCell class] forCellWithReuseIdentifier:@"Cell"];
    [self.myCollection registerNib:[UINib nibWithNibName:@"InvoiceSpreadTextCell" bundle:nil] forCellWithReuseIdentifier:@"Cell"];
    
    [self.myCollection registerClass:[InvoiceADD class] forCellWithReuseIdentifier:@"Cellb"];
    [self.myCollection registerNib:[UINib nibWithNibName:@"InvoiceADD" bundle:nil] forCellWithReuseIdentifier:@"Cellb"];
    
    [self.myCollection registerClass:[InvoiceEvidence class] forCellWithReuseIdentifier:@"CellE"];
    [self.myCollection registerNib:[UINib nibWithNibName:@"InvoiceEvidence" bundle:nil] forCellWithReuseIdentifier:@"CellE"];

    self.arrHeader = [[NSMutableArray alloc]initWithObjects:@"Item",@"Work Detail",@"Labour(£)",@"Materials(£)",@"Total(£)",@"Evidence", @"",nil];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 6;//no of row
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return self.noOfColumn;//no of column
}

- (BOOL) textField: (UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString: (NSString *)string {
    NSInteger sec = (textField.tag - 9001)/5;
    NSInteger row =  (textField.tag - 9001)%5;
    
    if(row==1 || row==2 ){
    
    NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
    [nf setNumberStyle:NSNumberFormatterNoStyle];
    
    NSString * newString = [NSString stringWithFormat:@"%@%@",textField.text,string];
    NSNumber * number = [nf numberFromString:newString];
    
    if (number)
        return YES;
    else
        return NO;
    }else{
        return YES;
    }
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
   
    if(textField==self.txtEmailTo){
        [UIView animateWithDuration:0.7 animations:^{
            CGRect r = self.view.frame;
            r.origin.y = r.origin.y - 300;
            self.view.frame=r;
        }];
    }
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField==self.txtEmailTo){
        [UIView animateWithDuration:0.7 animations:^{
            CGRect r = self.view.frame;
            r.origin.y = 0;
            self.view.frame=r;
        }];
        
       
    }
    else{
    NSInteger sec = (textField.tag - 9001)/5;
    NSInteger row =  (textField.tag - 9001)%5;
    if(row==2){
         NSString *strCellTag = [NSString stringWithFormat:@"%ld",(long)textField.tag];
         NSString *strLabourTag = [NSString stringWithFormat:@"%ld",(long)textField.tag -1];
         NSString *strTotTag = [NSString stringWithFormat:@"%ld",(long)textField.tag +1];
        
        
        float fltCellText = [textField.text floatValue];
        NSString *strfloatcellText = [NSString stringWithFormat:@"%0.2f",fltCellText];
        
        float tot = [[self.dict valueForKey:strLabourTag]floatValue] + fltCellText;
        NSString *strTot = [NSString stringWithFormat:@"%0.2f",tot];
        [self.dict setObject:strfloatcellText forKey:strCellTag];
        [self.dict setObject:strTot forKey:strTotTag];
        self.lastTag = tagText;
        [self.myCollection reloadData];
        [self reloadTotal];
        [self.view endEditing:YES];
        
    }else if (row==1){
        NSString *strCellTag = [NSString stringWithFormat:@"%ld",(long)textField.tag];
        float fltCellText = [textField.text floatValue];
        NSString *strfloatcellText = [NSString stringWithFormat:@"%0.2f",fltCellText];
        if([self.dict valueForKey:strCellTag] != nil ){
            [self.dict setObject:strfloatcellText forKey:strCellTag];
            textField.text = strfloatcellText;
        }
        
    }
    else{
    NSString *strCellTag = [NSString stringWithFormat:@"%ld",(long)textField.tag];
    
    if([self.dict valueForKey:strCellTag] != nil ){
       [self.dict setObject:textField.text forKey:strCellTag];
    }else{
        //[self.dict setObject:textField.text forKey:strCellTag];
    }
    }
    }
}

- (void)btnPopAction:(UIButton*)btn{
    [self.view endEditing:YES];
    self.imgTagCamera = btn.tag;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Evidence"
                                                    message:@""
                                                   delegate:self
                                          cancelButtonTitle:@"camera"
                                          otherButtonTitles:@"Gallery",nil];
    alert.tag = 6000;
    [alert show];
}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
     [self.view endEditing:YES];
    // the user clicked OK
    if(alertView.tag==6000){
    if (buttonIndex == 0) {
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePickerController.delegate = self;
        [self presentViewController:imagePickerController animated:YES completion:nil];
    }else{
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePickerController.delegate = self;
        [self presentViewController:imagePickerController animated:YES completion:nil];
    }
    }
}
// This method is called when an image has been chosen from the library or taken from the camera.
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
     [self.view endEditing:YES];
    //You can retrieve the actual UIImage
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    NSString *strCellTag = [NSString stringWithFormat:@"%ld",(long)self.imgTagCamera];
    [self.dict setObject:image forKey:strCellTag];

   [picker dismissViewControllerAnimated:YES completion:^{
        self.lastTag = tagText;
       [self.myCollection reloadData];
       [self reloadTotal];
   }];
}
- (void)btnInvoiceDelAction:(UIButton*)sender{
    
    
    if(self.noOfColumn >2){
    NSInteger sec = sender.tag - tagDelete - 1;
    
    NSInteger strtTag = 9001 + (sec * 5);
    
    NSString *strTag = [NSString stringWithFormat:@"%ld",(long)strtTag];
    
    NSInteger endTag = strtTag + 4;
    
    NSString *strEndTag = [NSString stringWithFormat:@"%ld",(long)endTag];
    
    NSMutableDictionary *dictswap = self.dict;
   
        
    
    [dictswap enumerateKeysAndObjectsUsingBlock:^(id key, id object, BOOL *stop) {
        NSLog(@"%@ = %@", key, object);
       NSInteger keyInteger = [key integerValue];
        if(keyInteger >= strtTag){
            
            if(keyInteger <= endTag){
                
               [dictswap removeObjectForKey:key];
            }else{
//                id value = [dictswap objectForKey:key];
//                [dictswap removeObjectForKey:key];
//                NSString *strNewKey = [NSString stringWithFormat:@"%ld",(keyInteger - 5)];
//                [dictswap setObject:value forKey:strNewKey];
            }
            
        }
   }];
        [dictswap enumerateKeysAndObjectsUsingBlock:^(id key, id object, BOOL *stop) {
            NSLog(@"%@ = %@", key, object);
            NSInteger keyInteger = [key integerValue];
           
                
                if(keyInteger > endTag){
                    
                   
                                  id value = [dictswap objectForKey:key];
                                  [dictswap removeObjectForKey:key];
                                  NSString *strNewKey = [NSString stringWithFormat:@"%ld",(keyInteger - 5)];
                                  [dictswap setObject:value forKey:strNewKey];
                }
                
            
        }];

    self.dict = dictswap;
    self.lastTag = tagText;
    self.noOfColumn = self.noOfColumn - 1;
    [self.myCollection reloadData];
    [self reloadTotal];
    }

}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section==0){
        InvoiceHeaderCell *cellH = [collectionView dequeueReusableCellWithReuseIdentifier:@"CellH" forIndexPath:indexPath];
        if(!cellH){
            [collectionView registerNib:[UINib nibWithNibName:@"InvoiceHeaderCell" bundle:nil] forCellWithReuseIdentifier:@"CellH"];
            [collectionView dequeueReusableCellWithReuseIdentifier:@"CellH" forIndexPath:indexPath];
        }
        NSLog(@"section=%ld&row=%ld",(long)indexPath.section,(long)indexPath.row);
        NSString *str= [self.arrHeader objectAtIndex:indexPath.row];
        cellH.lblHeader.text = str;
        cellH.backgroundColor=[UIColor whiteColor];
        return cellH;
        
    }else{
        if(indexPath.row==0){
            InvoiceADD *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cellb" forIndexPath:indexPath];
            if(!cell){
                [collectionView registerNib:[UINib nibWithNibName:@"InvoiceADD" bundle:nil] forCellWithReuseIdentifier:@"Cellb"];
                [collectionView dequeueReusableCellWithReuseIdentifier:@"Cellb" forIndexPath:indexPath];
            }
            if(indexPath.section ==self.noOfColumn -1){
                cell.btnInvoiceAdd.hidden = NO;
               
            }else{
                cell.btnInvoiceAdd.hidden = YES;
                
            }
            [cell.btnInvoiceAdd addTarget:self action:@selector(btnInvoiceAddAction) forControlEvents:UIControlEventTouchUpInside];
           
            cell.btnDelete.tag = tagDelete + indexPath.section;
            [cell.btnDelete addTarget:self action:@selector(btnInvoiceDelAction:) forControlEvents:UIControlEventTouchUpInside];
             cell.backgroundColor=[UIColor whiteColor];
            NSLog(@"section=%ld&row=%ld",(long)indexPath.section,(long)indexPath.row);
            return cell;

        }else if (indexPath.row ==5){
            cellE = [collectionView dequeueReusableCellWithReuseIdentifier:@"CellE" forIndexPath:indexPath];
            if(!cellE){
                [collectionView registerNib:[UINib nibWithNibName:@"InvoiceEvidence" bundle:nil] forCellWithReuseIdentifier:@"CellE"];
                [collectionView dequeueReusableCellWithReuseIdentifier:@"CellE" forIndexPath:indexPath];
            }
            
           
            
            
            cellE.btnPop.tag =tagText + (indexPath.row + (indexPath.section - 1)*5);
            cellE.tag = cellE.btnPop.tag;
            
            
            [cellE.btnPop addTarget:self action:@selector(btnPopAction:) forControlEvents:UIControlEventTouchUpInside];
            
            //populate val with dictionary
            NSString *strCellTag = [NSString stringWithFormat:@"%d",cellE.tag];
            
            if([self.dict valueForKey:strCellTag] != nil || ![[self.dict valueForKey:strCellTag]  isEqual: @""]){
                [cellE.btnPop setBackgroundImage:nil forState:UIControlStateNormal];
                UIImage *imgAgain = [self.dict objectForKey:strCellTag];
                [cellE.btnPop setBackgroundImage:imgAgain forState:UIControlStateNormal];
             
            }else{
                [cellE.btnPop setBackgroundImage:nil forState:UIControlStateNormal];
                //[self.dict setObject:@"" forKey:strCellTag];
            }
            
            
            NSLog(@"section=%ld&row=%ld",(long)indexPath.section,(long)indexPath.row);
            // NSString *str= [self.arrHeaderSheetList objectAtIndex:indexPath.section];
            cellE.backgroundColor=[UIColor whiteColor];
            //cell.lblContent.text = str;
            
            return cellE;

        }
        else{
            InvoiceSpreadTextCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
            if(!cell){
                [collectionView registerNib:[UINib nibWithNibName:@"InvoiceSpreadTextCell" bundle:nil] forCellWithReuseIdentifier:@"Cell"];
                [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
            }
            cell.txtInvoice.delegate = self;
            
            cell.txtInvoice.tag = tagText + (indexPath.row + (indexPath.section - 1)*5);
                cell.tag = cell.txtInvoice.tag;
            

            
            if(indexPath.row==4){
                cell.txtInvoice.enabled=false;
            }else{
                cell.txtInvoice.enabled=true;
            }
            
            
            //populate val with dictionary
            NSString *strCellTag = [NSString stringWithFormat:@"%d",cell.tag];
            
            if([self.dict valueForKey:strCellTag] != nil ){
                cell.txtInvoice.text = [self.dict valueForKey:strCellTag];
            }else{
               [self.dict setObject:cell.txtInvoice.text forKey:strCellTag];
            }
            
            
            
            NSLog(@"section=%ld&row=%ld",(long)indexPath.section,(long)indexPath.row);
           
            cell.backgroundColor=[UIColor whiteColor];
            return cell;

        }
        
    }
    
    
}

- (void)btnInvoiceAddAction{
    self.lastTag = tagText;
    self.noOfColumn = self.noOfColumn + 1;
    //Add value in dictionary
    
    NSInteger lastcol = self.noOfColumn - 1;
    
    NSInteger strtTag = tagText + (1 + (lastcol - 1)*5);
    NSInteger endTag = strtTag + 3;
    for(NSInteger i= strtTag; i<=endTag; i++){
        NSString *strTag = [NSString stringWithFormat:@"%ld",(long)i];
        [self.dict setObject:@"" forKey:strTag];
    }
    [self.myCollection reloadData];
    [self reloadTotal];
}
- (void)btnInvoiceMinusAction{
    self.lastTag = tagText;
    if(self.noOfColumn >2){
        self.noOfColumn = self.noOfColumn - 1;
    }
    
    [self.myCollection reloadData];
    [self reloadTotal];
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
        return CGSizeMake((self.myCollection.frame.size.width -12)/6, 50);
    
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0,0,2,2);//top - left - bottom- right
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)createDate{
    UIViewController* popoverContent = [[UIViewController alloc] init]; //ViewController
    
    UIView *popoverView = [[UIView alloc] init];   //view
    popoverView.backgroundColor = [UIColor clearColor];
    
    self.datePicker = [[UIDatePicker alloc]init];//Date picker
    self.datePicker.frame=CGRectMake(0,44,320, 216);
    self.datePicker.datePickerMode = UIDatePickerModeDate;
   
    [self.datePicker addTarget:self action:@selector(result) forControlEvents:UIControlEventValueChanged];//need to implement this method in same class
    [popoverView addSubview:self.datePicker];
    
    popoverContent.view = popoverView;
    UIPopoverController *popoverController = [[UIPopoverController alloc] initWithContentViewController:popoverContent];
    popoverController.delegate = self;
    
    [popoverController setPopoverContentSize:CGSizeMake(320, 264) animated:NO];
    [popoverController presentPopoverFromRect:self.btnAgent.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
     NSLog(@"%@",self.datePicker.date);
}
- (void)result{
    NSLog(@"%@",self.datePicker.date);
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd"];
    NSString *formattedDate = [df stringFromDate:self.datePicker.date];
    self.strJobDate = formattedDate;
}
- (IBAction)btnAgentAction:(UIButton *)sender {
    [self.view endEditing:YES];
   [self.view addSubview:self.popUp.view];
    
}
#pragma mark - ColorPickerDelegate method
-(void)selectedColor:(UIColor *)newColor
{
    self.lblAgent.text = newColor;
    
    //Dismiss the popover if it's showing.
    if (_colorPickerPopover) {
        [_colorPickerPopover dismissPopoverAnimated:YES];
        _colorPickerPopover = nil;
    }
}

- (IBAction)btnBackAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)resultJobDt{
    NSLog(@"%@",self.datePicker.date);
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd/MM/YYYY"];
    NSString *formattedDate = [df stringFromDate:self.datePicker.date];
    self.strJobDate = formattedDate;
    self.lblJobDt.text = self.strJobDate;
}
- (void)resultDt{
    NSLog(@"%@",self.datePicker.date);
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
     [df setDateFormat:@"dd/MM/YYYY"];
   // [df setDateFormat:@"yyyy-MM-dd"];
    NSString *formattedDate = [df stringFromDate:self.datePicker.date];
    self.strDate = formattedDate;
    self.lblDate.text = self.strDate;
}
- (IBAction)btnJobDtAction:(UIButton *)sender {
    [self.view endEditing:YES];
    UIViewController* popoverContent = [[UIViewController alloc] init]; //ViewController
    
    UIView *popoverView = [[UIView alloc] init];   //view
    popoverView.backgroundColor = [UIColor clearColor];
    
    self.datePicker = [[UIDatePicker alloc]init];//Date picker
    self.datePicker.frame=CGRectMake(0,44,320, 216);
    self.datePicker.datePickerMode = UIDatePickerModeDate;
    
    [self.datePicker addTarget:self action:@selector(resultJobDt) forControlEvents:UIControlEventValueChanged];//need to implement this method in same class
    [popoverView addSubview:self.datePicker];
    
    popoverContent.view = popoverView;
    UIPopoverController *popoverController = [[UIPopoverController alloc] initWithContentViewController:popoverContent];
    popoverController.delegate = self;
    
    [popoverController setPopoverContentSize:CGSizeMake(320, 216) animated:NO];
    [popoverController presentPopoverFromRect:CGRectMake(0, 0, 1700, 0) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];

}

- (IBAction)btnDateAction:(UIButton *)sender {
    [self.view endEditing:YES];
    UIViewController* popoverContent = [[UIViewController alloc] init]; //ViewController
    
    popoverContent.popoverPresentationController.backgroundColor = [UIColor whiteColor];
    
    
    popoverContent.view.backgroundColor=[UIColor clearColor];
    UIView *popoverView = [[UIView alloc] init];   //view
    popoverView.backgroundColor = [UIColor clearColor];
    
    self.datePicker = [[UIDatePicker alloc]init];//Date picker
    self.datePicker.frame=CGRectMake(0,44,320, 216);
    self.datePicker.datePickerMode = UIDatePickerModeDate;
    
    [self.datePicker addTarget:self action:@selector(resultDt) forControlEvents:UIControlEventValueChanged];//need to implement this method in same class
    [popoverView addSubview:self.datePicker];
    
    popoverContent.view = popoverView;
    UIPopoverController *popoverController = [[UIPopoverController alloc] initWithContentViewController:popoverContent];
    popoverController.delegate = self;
    
    [popoverController setPopoverContentSize:CGSizeMake(320, 216) animated:NO];
    [popoverController presentPopoverFromRect:CGRectMake(0, 0, 1700, 800) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];

}
- (void)createInvoice{
    //    agentid,empid,customer_ref,name,address,job_date,date,email,
    //    data(array)[worksdetail,labour,materials,total,evidence,vat]
    
    self.strCustomRef = self.txtCustomRef.text;
    self.strAddress = self.txtAddr.text;
    self.strJobDate = self.lblJobDt.text;
    self.strDate = self.lblDate.text;
    self.strEmail = self.txtEmailTo.text;
    
    self.strName = self.txtName.text;
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *strUid = [prefs stringForKey:@"uid"];
    
    self.strEmpID = strUid;
    self.strWebservice = [NSString stringWithFormat:@"agentid=%@&empid=%@&customer_ref=%@&name=%@&address=%@&job_date=%@&date=%@&email=%@",self.strAgent,self.strEmpID,self.strCustomRef,self.strName,self.strAddress,self.strJobDate,self.strDate,self.strEmail];
    
    
    //data(array)[worksdetail,labour,materials,total,evidence,vat]
}
- (void)submitValByDict:(NSMutableArray*)arr{
    [activity showView];
       // NSMutableArray *arr = [[NSMutableArray alloc]initWithCapacity:40];
    
       
    NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:arr options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
    NSLog(@"jsonData as string:\n%@", jsonString);
    
    
    
    //dateformat change
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
     [df setDateFormat:@"dd/MM/YYYY"];
    NSDate *jobdt = [df dateFromString:self.strJobDate];
    [df setDateFormat:@"yyyy-MM-dd"];
    self.strJobDate = [df stringFromDate:jobdt];
    [df setDateFormat:@"dd/MM/YYYY"];
    NSDate *dt = [df dateFromString:self.strDate];
    [df setDateFormat:@"yyyy-MM-dd"];
    self.strDate = [df stringFromDate:dt];
    
    
    // Dictionary that holds post parameters. You can set your post parameters that your server accepts or programmed to accept.
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
   
    [ _params setObject:self.strAgent forKey:@"agentid"];
    [ _params setObject:self.strEmpID forKey:@"empid"];
    [ _params setObject:self.strCustomRef forKey:@"customer_ref"];
    [ _params setObject:self.strName forKey:@"name"];
    [ _params setObject:self.strAddress forKey:@"address"];
    [ _params setObject:self.strJobDate forKey:@"job_date"];
    [ _params setObject:self.strDate forKey:@"date"];
    [ _params setObject:self.strEmail forKey:@"email"];
    [ _params setObject:self.strSubTotal forKey:@"subtotal"];
    [ _params setObject:self.strVat forKey:@"vat"];
    [ _params setObject:self.strTotal forKey:@"total"];
    [ _params setObject:self.strInvoiceNo forKey:@"invoice_number"];
    [ _params setObject:jsonString forKey:@"data"];
    
    // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
    NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
    
    // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
    NSString* FileParamConstant = @"file";
    
    NSString *strLoginUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,kInvoiceCreateNew];
    // the server url to which the image (or the media) is uploaded. Use your server url here
    NSURL* requestURL = [NSURL URLWithString:strLoginUrl];
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:1600];
    [request setHTTPMethod:@"POST"];
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    
    //Save Signature
    
    UIGraphicsBeginImageContext(self.signatureView.frame.size);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    [[UIColor blackColor] set];
    CGContextFillRect(ctx, self.signatureView.frame);
    
    [self.signatureView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *vwImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
     NSData *imageData = UIImageJPEGRepresentation(vwImage, 0.2f);
    
    if (imageData) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", @"image"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imageData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    
     //Add Evidence Data
    for (NSInteger a=9001; a< 9040; a++) {
        
        NSString *strCellTag = [NSString stringWithFormat:@"%ld",(long)a];
        NSInteger sec = (a - 9001)/5;
        NSInteger row =  (a - 9001)%5;
        if(row==4){
            if([self.dict valueForKey:strCellTag] != nil ){
                
                UIImage *imgAgain = [self.dict objectForKey:strCellTag];
                //NSData *imageData = UIImagePNGRepresentation(imgAgain);
                NSData *data = UIImageJPEGRepresentation(imgAgain, 0.2f);
                [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", @"evidence[]"] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:data];
                [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
                
            }
            
        }
        
    }

    
    
   [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    NSString *postLength = [NSString stringWithFormat:@"%d", [body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setURL:requestURL];
    

    
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data1, NSError *connectionError) {
        [activity hideView];
        if(data1)
        {
          //  NSString *returnString = [[NSString alloc] initWithData:data1 encoding:NSUTF8StringEncoding];
            //NSDictionary *res = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            NSError *jsonParserError;
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data1 options:kNilOptions error:&jsonParserError];
           // NSLog(@"res ::%@", returnString);
            [self clearAllField];
            
            NSInteger status = [[[json valueForKey:@"result"]valueForKey:@"status"]integerValue];
            if(status==1){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Invoice Creation"
                                                                message:@"Invoice Creation successful"
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                alert.tag = 5400;
                
                [alert show];
                
            }else{
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Invoice Creation"
                                                                message:@"Some Problem Occur!!!Please try again later!!"
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                 alert.tag = 5401;
                [alert show];
            }

            
            //NSInteger success = [[res objectForKey:@"success"]integerValue];
            
        }
        else{
            
        }
        // block(NO);
    }];
}


- (IBAction)btnConfirmAction:(UIButton *)sender {
    //Assign Single parametere
   
    self.strCustomRef = self.txtCustomRef.text;
    self.strAddress = self.txtAddr.text;
    self.strJobDate = self.lblJobDt.text;
    self.strDate = self.lblDate.text;
    self.strEmail = self.txtEmailTo.text;
    self.strName = self.txtName.text;
    
    self.strInvoiceNo = self.txtInvoiceNo.text;
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    
    
    self.lastTag = tagText + (self.noOfColumn - 1)* 5;
    for(int x= 9001;x<=self.lastTag;x=x+5){
        NSMutableDictionary* p = [[NSMutableDictionary alloc] init];
        for(int z=x;z<(x+5);z=z+1){
            NSString *strTag = [NSString stringWithFormat:@"%d",z];
            NSString *strText = [self.dict valueForKey:strTag];
            NSInteger row =  (z - 9001)%5;
            if(row==0){
                [p setObject:strText forKey:@"worksdetail"];
            }else if (row==1){
                [p setObject:strText forKey:@"labour"];
            }else if (row==2){
                [p setObject:strText forKey:@"materials"];
            }else if (row==3){
                [p setObject:strText forKey:@"sheettotal"];
            }
            
        }
        [arr addObject:p];
    }

    
    //self.strInvoiceNo= @"001/2345";
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *strUid = [prefs stringForKey:@"uid"];
    
    self.strEmpID = strUid;
    
    NSString *emailReg = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailReg];
    
    
    
    if([self.strName isEqualToString:@""] || self.strName ==nil){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Invoice Creation"
                                                        message:@"Please enter Name"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        
        [alert show];
    }else if([self.strCustomRef isEqualToString:@""] || self.strCustomRef ==nil){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Invoice Creation"
                                                        message:@"Please enter Customer reference No"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        
        [alert show];
    }else if([self.strAddress isEqualToString:@""] || self.strAddress ==nil){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Invoice Creation"
                                                        message:@"Please enter Address"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        
        [alert show];
    }else if([self.strJobDate isEqualToString:@""] || self.strJobDate ==nil || [self.strJobDate isEqualToString:@"Select Job Date"]  ){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Invoice Creation"
                                                        message:@"Please enter Job Date"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        
        [alert show];
    }else if([self.strDate isEqualToString:@""] || self.strDate ==nil  || [self.strDate isEqualToString:@"Select  Date"] ){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Invoice Creation"
                                                        message:@"Please enter Date"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        
        [alert show];
    }else if([self.strAgent isEqualToString:@""] || self.strAgent ==nil){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Invoice Creation"
                                                        message:@"Please enter Agent"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        
        [alert show];
    }else if([self.strInvoiceNo isEqualToString:@""] || self.strInvoiceNo ==nil){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Invoice Creation"
                                                        message:@"Please enter Invoice No"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        
        [alert show];
    }else if ([self.strTotal isEqualToString:@""] || self.strTotal == nil){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Invoice Creation"
                                                        message:@"Please enter work details"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        
        [alert show];
    }else if ([emailTest evaluateWithObject:self.strEmail] == NO)
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Enter valid Email" message:@"Please Enter Valid Email Address." delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
        [alert show];
        
    }else if (arr.count==0){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Work Details" message:@"Please Enter Work Details" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
        [alert show];
    }

    else{
        [self submitValByDict:arr];
    }
    
    
    
    
}
- (void)clearAllField{
    
    [self.dict removeAllObjects];
    self.noOfColumn = 3;
    //Add value in dictionary
    NSInteger strtTag = tagText + 1;
    NSInteger endTag = tagText + (self.noOfColumn - 1)* 5;
    for(NSInteger i= strtTag; i<=endTag; i++){
         NSInteger row =  (i - 9000)%5;
        if(row==0){
            
           
        }else{
            NSString *strTag = [NSString stringWithFormat:@"%ld",(long)i];
            [self.dict setObject:@"" forKey:strTag];
        }
       
    }
    [self.myCollection reloadData];
    

    self.lastTag = tagText;
    [self.myCollection reloadData];
    //[self reloadTotal];
    self.txtCustomRef.text = @"";
    self.txtAddr.text = @"" ;
    self.lblJobDt.text = @"";
    self.lblDate.text = @"";
    self.txtEmailTo.text = @"" ;
    
    self.txtName.text = @"";
    self.txtInvoiceNo.text = @"";
    self.lblAgent.text = @"";
    self.lblSubTot.text = @"";
    self.lblVat.text = @"";
    self.lblTot.text = @"";
    
    [self.signatureView erase];
    
    
    

}
- (IBAction)btnClearAction:(UIButton *)sender {
    
    [self.dict removeAllObjects];
    self.lastTag = tagText;
    [self.myCollection reloadData];
    //[self reloadTotal];
    self.txtCustomRef.text = @"";
    self.txtAddr.text = @"" ;
    self.lblJobDt.text = @"";
    self.lblDate.text = @"";
    self.txtEmailTo.text = @"" ;
    
    self.txtName.text = @"";
   
}

//url : api/createinvoicenew
//method : POST
//data : agentid,empid,customer_ref,name,address,job_date,date,email, subtotal, vat, total
//data(array)[worksdetail,labour,materials,sheettotal]
//evidence (evidence)

@end
