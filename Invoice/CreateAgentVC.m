//
//  CreateAgentVC.m
//  Invoice
//
//  Created by MAPPS MAC on 24/02/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "CreateAgentVC.h"
#import "WebServiceManager.h"
#import "POAcvityView.h"
#import "AppDelegate.h"
@interface CreateAgentVC (){
    POAcvityView *activity;
     AppDelegate *delegate;
}
@property(nonatomic)NSString *strEmail;
@property(nonatomic)NSString *strPass;
@property (weak, nonatomic) IBOutlet UITextField *txtUserNmae;

@property (weak, nonatomic) IBOutlet UITextField *txtPass;
- (IBAction)btnBackAction:(UIButton *)sender;

@end

@implementation CreateAgentVC
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    [self.view endEditing:true];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [singleFingerTap setCancelsTouchesInView:NO];
    [self.view addGestureRecognizer:singleFingerTap];
    activity = [[POAcvityView alloc]initWithTitle:@"Loading.." message:@"Loading.."];
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnLoginAction:(UIButton *)sender {
    //[self performSegueWithIdentifier:@"segueInvoice" sender:self];
    if([self.txtUserNmae.text isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Create Agent"
                                                        message:@"Please enter Agent Name"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
    }else if ([self.txtPass.text isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Create Agent"
                                                        message:@"Please enter Agent Email"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }else{

    [activity showView];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *strUid = [prefs stringForKey:@"uid"];
    [WebServiceManager agentCreation:strUid andEmpName:self.txtUserNmae.text andEmpMail:self.txtPass.text onCompletion:^(id object, NSError *error) {
        if(object){
             [WebServiceManager getAgentList:strUid onCompletion:^(id object, NSError *error) {
                 [activity hideView];
                 if(object){
                     delegate.arrAgentList = object;
                      [self performSegueWithIdentifier:@"segueDashboard" sender:self];
                 }else{
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Invoice"
                                                                     message:@"Some Problem Occurs!!!Please try again later"
                                                                    delegate:self
                                                           cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil];
                     [alert show];
                     
                 }
             }];

           
         }else{
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Create Agent"
                                                             message:@"Some Problem Occurs!!!Please try again later"
                                                            delegate:self
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
             [alert show];
         }
    }];
    
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnBackAction:(UIButton *)sender {
    [activity showView];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *strUid = [prefs stringForKey:@"uid"];
    [WebServiceManager getAgentList:strUid onCompletion:^(id object, NSError *error) {
        [activity hideView];
        if(object){
            delegate.arrAgentList = object;
            [self performSegueWithIdentifier:@"segueDashboard" sender:self];
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Invoice"
                                                            message:@"Some Problem Occurs!!!Please try again later"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
    }];

    
}
@end
