//
//  InvoiceEditVC.m
//  Invoice
//
//  Created by Aditi on 10/04/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "InvoiceEditVC.h"
#import "InvoiceHeaderCell.h"
#import "InvoiceEvidence.h"
#import "InvoiceADD.h"
#import "InvoiceSpreadTextCell.h"
#import "WebServiceManager.h"
#import "POAcvityView.h"
#import "AppDelegate.h"
#import "PopUpAgent.h"
#import "Agent.h"
#import "SignatureViewQuartzQuadratic.h"
#import "AppConstants.h"

@interface InvoiceEditVC ()<UITextFieldDelegate>{
    InvoiceEvidence *cellE;
}
@property (weak, nonatomic) IBOutlet SignatureViewQuartzQuadratic *signatureView;

- (IBAction)btnClearAction:(UIButton *)sender;
@property(nonatomic)UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UICollectionView *myCollection;
- (IBAction)btnAgentAction:(UIButton *)sender;
@property(nonatomic) NSMutableArray *arrHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblAgent;
- (IBAction)btnBackAction:(UIButton *)sender;
@property(nonatomic)NSInteger noOfColumn;
@property(nonatomic)PopUpAgent *popUp;
@property(nonatomic)NSInteger maxTag;
@property(nonatomic)NSInteger lastTag;
@property(nonatomic)NSString *strWebservice;
#define  tagText 9000
@property (weak, nonatomic) IBOutlet UILabel *lblJobDt;
- (IBAction)btnJobDtAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UITextField *textCustomerRef;
- (IBAction)btnDateAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnDate;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UITextField *txtEmailTo;
- (IBAction)btnConfirmAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet SignatureViewQuartzQuadratic *myDigitalView;
@property(nonatomic)NSMutableDictionary *dict;
@property(nonatomic)NSInteger imgTagCamera;
@property (weak, nonatomic) IBOutlet UIView *viewDt;

@property(nonatomic)NSString *strSubTotal;
@property(nonatomic)NSString *strVat;
@property(nonatomic)NSString *strTotal;
@property (weak, nonatomic) IBOutlet UILabel *lblSubTot;
@property (weak, nonatomic) IBOutlet UILabel *lblVat;
@property (weak, nonatomic) IBOutlet UILabel *lblTot;
- (IBAction)btnCancelAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imgSignature;

@property (weak, nonatomic) IBOutlet UITextField *txtInvoice;

@end

@implementation InvoiceEditVC
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    [self.view endEditing:true];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    //The setup code (in viewDidLoad in your view controller)
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [singleFingerTap setCancelsTouchesInView:NO];
    [self.view addGestureRecognizer:singleFingerTap];
    self.arrInvoiceDetails = [[NSMutableArray alloc]init];
    self.arrEvidence = [[NSMutableArray alloc]init];
    
    self.txtInvoice.text = self.job.strInvoiceNum;
    
    self.lblAgent.text = self.job.strAgentName;
    
    self.arrInvoiceDetails = self.job.arrInvoiceDetails;
    self.arrEvidence = self.job.arrEvidence;
    
    
    self.lblSubTot.text = [NSString stringWithFormat:@"%0.2f",[self.job.strSubTot floatValue]];
    self.lblVat.text = [NSString stringWithFormat:@"%0.2f",[self.job.strVat floatValue]];
    self.lblTot.text = [NSString stringWithFormat:@"%0.2f",[self.job.strTot floatValue]];
    
    
    self.txtCustomRef.text = self.job.strCust;
    self.txtAddr.text = self.job.strAddr;
    self.lblJobDt.text = self.job.strJobDt;
    self.lblDate.text = self.job.strDate;
    self.txtEmailTo.text = self.job.strEmail;
    
    self.txtName.text = self.job.strName;
    
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^{
        
       
        NSString *str = self.job.strsignature;
        NSURL *imageURL= [NSURL URLWithString:str];
        NSData *data = [NSData dataWithContentsOfURL:imageURL];
        
        UIImage *image = [UIImage imageWithData:data];
        dispatch_async(dispatch_get_main_queue(), ^{
            self.imgSignature.image = image;
        });
    });


    
    [self.myCollection registerClass:[InvoiceHeaderCell class] forCellWithReuseIdentifier:@"CellH"];
    [self.myCollection registerNib:[UINib nibWithNibName:@"InvoiceHeaderCell" bundle:nil] forCellWithReuseIdentifier:@"CellH"];
    
    [self.myCollection registerClass:[InvoiceSpreadTextCell class] forCellWithReuseIdentifier:@"Cell"];
    [self.myCollection registerNib:[UINib nibWithNibName:@"InvoiceSpreadTextCell" bundle:nil] forCellWithReuseIdentifier:@"Cell"];
    
    [self.myCollection registerClass:[InvoiceADD class] forCellWithReuseIdentifier:@"Cellb"];
    [self.myCollection registerNib:[UINib nibWithNibName:@"InvoiceADD" bundle:nil] forCellWithReuseIdentifier:@"Cellb"];
    
    [self.myCollection registerClass:[InvoiceEvidence class] forCellWithReuseIdentifier:@"CellE"];
    [self.myCollection registerNib:[UINib nibWithNibName:@"InvoiceEvidence" bundle:nil] forCellWithReuseIdentifier:@"CellE"];
    //    [self.myCollection registerClass:[InvoiceEdit class] forCellWithReuseIdentifier:@"CellE"];
    //    [self.myCollection registerNib:[UINib nibWithNibName:@"InvoiceEdit" bundle:nil] forCellWithReuseIdentifier:@"CellE"];
    
    self.arrHeader = [[NSMutableArray alloc]initWithObjects:@"Item",@"Work Detail",@"Labour(£)",@"Materials(£)",@"Total(£)",@"Evidence", @"",nil];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 6;//no of row
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return self.arrInvoiceDetails.count + 1;//no of column
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section==0){
        InvoiceHeaderCell *cellH = [collectionView dequeueReusableCellWithReuseIdentifier:@"CellH" forIndexPath:indexPath];
        if(!cellH){
            [collectionView registerNib:[UINib nibWithNibName:@"InvoiceHeaderCell" bundle:nil] forCellWithReuseIdentifier:@"CellH"];
            [collectionView dequeueReusableCellWithReuseIdentifier:@"CellH" forIndexPath:indexPath];
        }
        NSLog(@"section=%ld&row=%ld",(long)indexPath.section,(long)indexPath.row);
        NSString *str= [self.arrHeader objectAtIndex:indexPath.row];
        cellH.lblHeader.text = str;
        cellH.backgroundColor=[UIColor whiteColor];
        return cellH;
        
    }else{
        if(indexPath.row==0){
            InvoiceADD *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cellb" forIndexPath:indexPath];
            if(!cell){
                [collectionView registerNib:[UINib nibWithNibName:@"InvoiceADD" bundle:nil] forCellWithReuseIdentifier:@"Cellb"];
                [collectionView dequeueReusableCellWithReuseIdentifier:@"Cellb" forIndexPath:indexPath];
            }
            if(indexPath.section ==1){
                cell.btnInvoiceAdd.hidden = NO;
                cell.btnInvoiceMinus.hidden = NO;
                
                cell.btnInvoiceAdd.enabled = NO;
                cell.btnInvoiceMinus.enabled = NO;
            }else{
                cell.btnInvoiceAdd.hidden = YES;
                cell.btnInvoiceMinus.hidden = YES;
            }
           
            cell.backgroundColor=[UIColor whiteColor];
            NSLog(@"section=%ld&row=%ld",(long)indexPath.section,(long)indexPath.row);
            return cell;
            
        }else if (indexPath.row ==5){
            cellE = [collectionView dequeueReusableCellWithReuseIdentifier:@"CellE" forIndexPath:indexPath];
            if(!cellE){
                [collectionView registerNib:[UINib nibWithNibName:@"InvoiceEvidence" bundle:nil] forCellWithReuseIdentifier:@"CellE"];
                [collectionView dequeueReusableCellWithReuseIdentifier:@"CellE" forIndexPath:indexPath];
            }
            
            
            cellE.tag = (indexPath.section  + indexPath.row);
            //cellE.btnPop.tag= cellE.tag;
             //[cellE.btnPop setBackgroundImage:nil forState:UIControlStateNormal];
            NSLog(@"Tag==%ld",cellE.tag);
            if(self.arrEvidence.count !=0){
            NSDictionary *dict = [self.arrEvidence objectAtIndex:indexPath.section - 1];
            NSString *str = [dict valueForKey:@"evidence"];
            NSURL *imageURL= [NSURL URLWithString:str];
            NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:imageURL completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                if (data) {
                    UIImage *image = [UIImage imageWithData:data];
                    if (image) {
                    [cellE.btnPop setBackgroundImage:image forState:UIControlStateNormal];

                    }
                }
            }];
            [task resume];

            }
            
            NSLog(@"section=%ld&row=%ld",(long)indexPath.section,(long)indexPath.row);
            // NSString *str= [self.arrHeaderSheetList objectAtIndex:indexPath.section];
            cellE.backgroundColor=[UIColor whiteColor];
            //cell.lblContent.text = str;
            
            return cellE;
            
        }
        else{
            InvoiceSpreadTextCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
            if(!cell){
                [collectionView registerNib:[UINib nibWithNibName:@"InvoiceSpreadTextCell" bundle:nil] forCellWithReuseIdentifier:@"Cell"];
                [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
            }
            cell.txtInvoice.delegate = self;
            
            cell.txtInvoice.tag = self.lastTag + 1;
            cell.tag = cell.txtInvoice.tag;
            self.lastTag = cell.txtInvoice.tag;
            
            NSDictionary *dict = [self.arrInvoiceDetails objectAtIndex:indexPath.section - 1];
            
            
             cell.txtInvoice.enabled=false;
            if(indexPath.row==1){
                cell.txtInvoice.text = [dict valueForKey:@"worksdetail"];
            }else if(indexPath.row==2){
                NSString *strLabour = [NSString stringWithFormat:@"%0.2f",[[dict valueForKey:@"labour"]floatValue]];
                cell.txtInvoice.text = strLabour;
            }else if(indexPath.row==3){
                 NSString *strMaterial = [NSString stringWithFormat:@"%0.2f",[[dict valueForKey:@"materials"]floatValue]];
                cell.txtInvoice.text = strMaterial;
            }else if(indexPath.row==4){
                NSString *strSheet = [NSString stringWithFormat:@"%0.2f",[[dict valueForKey:@"sheettotal"]floatValue]];
                cell.txtInvoice.text = strSheet;
            }
            
            
            //populate val with dictionary
            NSString *strCellTag = [NSString stringWithFormat:@"%d",cell.tag];
            
            
            
            NSLog(@"section=%ld&row=%ld",(long)indexPath.section,(long)indexPath.row);
            
            cell.backgroundColor=[UIColor whiteColor];
            return cell;
            
        }
        
    }
    
    
}



-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((self.myCollection.frame.size.width -12)/6, 50);
    
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0,0,2,2);//top - left - bottom- right
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


//url : api/createinvoicenew
//method : POST
//data : agentid,empid,customer_ref,name,address,job_date,date,email, subtotal, vat, total
//data(array)[worksdetail,labour,materials,sheettotal]
//evidence (evidence)
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnCancelAction:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
