//
//  Agent.m
//  Invoice
//
//  Created by Aditi on 08/03/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "Agent.h"

@implementation Agent
- (id)initWithDictionary:(NSDictionary*)dict{
    self.strEmail= [dict valueForKey:@"email"];
    self.strName = [dict valueForKey:@"fullname"];
    self.strID = [NSString stringWithFormat:@"%ld",[[dict valueForKey:@"id"]integerValue]];
    return self;
}
- (id)createAgent:(NSInteger)index{
    if(index==0){
        self.strEmail= @"animesh@gmail.com";
        self.strName = @"Animesh Roy";
        self.strID = @"1";

    }else if (index==1){
        self.strEmail= @"tridib@gmail.com";
        self.strName = @"Tridib Sen";
        self.strID = @"2";

    }else{
        self.strEmail= @"anindita@gmail.com";
        self.strName = @"Anindita Sen";
        self.strID = @"3";

    }
    return self;
}
@end
