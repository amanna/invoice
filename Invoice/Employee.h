//
//  Employee.h
//  Invoice
//
//  Created by Aditi on 15/03/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Employee : NSObject
@property(nonatomic)NSString *strFName;
@property(nonatomic)NSString *strLName;
@property(nonatomic)NSString *strEmail;
@property(nonatomic)NSString *strID;
@property(nonatomic)NSString *strUserName;
@property(nonatomic)NSString *strPass;
@property(nonatomic)NSString *strAdmin;
- (id)initWithDictionary:(NSDictionary*)dict;
@end
