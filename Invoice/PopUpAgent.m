//
//  PopUpAgent.m
//  Invoice
//
//  Created by Aditi on 20/03/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "PopUpAgent.h"
#import "AppDelegate.h"
#import "Agent.h"
@interface PopUpAgent (){
     AppDelegate *delegate;
}
@property (weak, nonatomic) IBOutlet UITableView *myTbl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *htconstant;

@end

@implementation PopUpAgent

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    self.htconstant.constant = (delegate.arrAgentList.count) * 44;
    
    self.myTbl.frame = CGRectMake(self.myTbl.frame.origin.x,self.myTbl.frame.origin.y,self.myTbl.frame.size.width,self.htconstant.constant);
    self.myTbl.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    // Do any additional setup after loading the view.
}
- (void)setEventOnCompletion:(EventCompletionHandlerH)handler{
    eventCompletionHandlerH = handler;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Table View Data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:
(NSInteger)section{
    return delegate.arrAgentList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:
(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"cellID";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:
                             cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:
                UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    Agent *agent = [delegate.arrAgentList objectAtIndex:indexPath.row];
    cell.textLabel.text = agent.strName;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
     Agent *agent = [delegate.arrAgentList objectAtIndex:indexPath.row];
    eventCompletionHandlerH(agent,kActionSelect);
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
